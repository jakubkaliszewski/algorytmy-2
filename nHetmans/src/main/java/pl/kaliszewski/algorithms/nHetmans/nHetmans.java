package pl.kaliszewski.algorithms.nHetmans;

/**
 * Klasa rozwiązująca problem 8 hetmanów.
 */
public class nHetmans {
    /**
     * Stan ustawienia hetmanów na szachownicy.
     */
    private byte[] state;
    /**
     * Określa czy w i-tym wierszu znajduje się hetman.
     */
    private boolean[] rowLock;
    /**
     * Określa czy miejsce w diagonali / jest zajęte przez hetmana;
     */
    private boolean[] diagonalNW;
    /**
     * Określa czy miejsce w diagonali \ jest zajęte przez hetmana;
     */
    private boolean[] diagonalNE;
    /**
     * Określa liczbę kolumn oraz wierszy na szachownicy.
     */
    private byte size;
    /**
     * Określa czy rozwiązano problem.
     */
    private boolean solved;

    public nHetmans(byte size){
        this.size = size;
        rowLock = new boolean[size];
        state = new byte[size];
        diagonalNE = new boolean[size*2 - 1];
        diagonalNW = new boolean[size*2 - 1];
    }

    /**
     * Rozwiązuje problem n-hetmanów.
     * Wyświetla rozwiązanie na ekranie konsoli.
     */
    public void prepareSolution(){
        if(solved){
            showResult();
            return;
        }

        byte hetmanIndex = 0;
        tryGetSolution(hetmanIndex);
        showResult();
    }


    private void tryGetSolution(byte hetmanIndex){
        byte row = -1;
        do  {
            row++;
            if(checkSpace(row, hetmanIndex)){
                //można ustawić hetmana w tym miejscu
                setHetmanOnPosition(row, hetmanIndex);

                if(hetmanIndex < size -1){
                    tryGetSolution((byte) (hetmanIndex + 1));
                    if(!solved)
                        removeHetmanFromPosition(row, hetmanIndex);
                }else{
                    solved = true;
                }
            }
        }while(row != size - 1 && !solved);

    }

    /**
     * Ustawia hetmana na zadanej pozycji.
     * @param row Indeks wiersza.
     * @param column Indeks kolumny.
     */
    private void setHetmanOnPosition(byte row, byte column){
        state[column] = row;
        rowLock[row] = true;
        diagonalNW[row + column] = true;
        diagonalNE[column - row + (size - 1)] = true;
    }

    /**
     * Usuwa Hetmana z zadanej pozycji.
     * @param row Indeks wiersza.
     * @param column Indeks kolumny.
     */
    private void removeHetmanFromPosition(byte row, byte column){
        rowLock[row] = false;
        diagonalNW[row + column] = false;
        diagonalNE[column - row + (size -1)] = false;
    }

    /**
     * Sprawdza czy pod zadaną pozycją można umieścić Hetmana.
     * @param row Indeks wiersza.
     * @param column Indeks kolumny.
     * @return Rezultat czy pozycja jest dostępna.
     */
    private boolean checkSpace(byte row, byte column){
        //sprawdzanie wolności w wierszu
        if(rowLock[row])
            return false;
        //sprawdzenie wolności w przekątnej NW
        if(diagonalNW[row + column])
            return false;
        //sprawdzenie wolności w przekątnej NE
        if(diagonalNE[column - row + (size -1)])
            return false;
        return true;
    }

    /**
     * W przystępny sposób wyświetla rozwiązanie.
     */
    public void showResult(){
        if (solved) {
            var result = this.toString();
            System.out.println(result);
        }else{
            System.out.println("Problem n hetmanów nie jest (jeszcze) rozwiązany!");
        }
    }

    @Override
    public String toString() {
        //Zwraca String prezentujący aktualny stan szachownicy
        StringBuilder builder = new StringBuilder();

        for (byte row = 0; row < size; row++) {
            builder.append("| ");
            for (byte column = 0; column < size; column++){
                if(state[column] == row)
                    builder.append("X");
                else
                    builder.append(" ");
                builder.append(" | ");
            }
            builder.append("\n");
        }

        return builder.toString();
    }
}
