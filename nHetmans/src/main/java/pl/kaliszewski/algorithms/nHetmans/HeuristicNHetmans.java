package pl.kaliszewski.algorithms.nHetmans;

import Permutation.PermutationGenerator;


/**
 * Klasa rozwiązująca problem N hetmanów z heurystyką.
 * Metoda ta posiada dużo wyższą skuteczność dla dużych rozmiarów szachownicy.
 */
public class HeuristicNHetmans {
    /**
     * Stan ustawienia hetmanów na szachownicy.
     */
    private int[] actualState;
    /**
     * Określa liczbę kolumn oraz wierszy na szachownicy.
     */
    private int size;
    /**
     * Określa czy rozwiązano problem.
     */
    private boolean solved;

    public HeuristicNHetmans(int size){
        this.size = size;
        generateRandomState();
    }

    /**
     * Rozwiązuje problem n-hetmanów z heurystyką.
     * Wyświetla rozwiązanie na ekranie konsoli.
     */
    public void prepareSolution(){
        if(solved){
            showResult();
            return;
        }

        int conflicts = getConflictsCount(this.actualState);
        System.out.printf("Początkowa liczba konfliktów wynosi: %d\n", conflicts);

        int iteration = 1;
        boolean changed;

        do{
            changed = false;
            for (int column = 0; column < size; column++) {
                var actualHetmanRow = actualState[column];
                for (int nextColumn = column + 1; nextColumn < size; nextColumn++) {
                    var nextHetmanRow = actualState[nextColumn];
                    flipHetmans(actualHetmanRow, nextHetmanRow, actualState);
                    int tempConflicts = getConflictsCount(actualState);
                    if (tempConflicts < conflicts){
                        conflicts = tempConflicts;
                        changed = true;
                        break;
                    }else{
                        flipHetmans(actualHetmanRow, nextHetmanRow, actualState);
                    }
                }
            }

            conflicts = getConflictsCount(actualState);
            System.out.printf("Liczba konfliktów w przebiegu %d wynosi: %d\n", iteration, conflicts);
            iteration++;

            if (!changed){
                iteration = 1;
                generateRandomState();
                System.out.println("Wylosowano nowe ustawienie!");
            }
        }while(conflicts > 0);
        solved = true;

        showResult();
    }

    /**
     * Dokonuje zamiany pozycji (wiersze) między dwoma wskazanymi hetmanami.
     * @param firstHetman Kolumna odpowiadająca 1. szemu hetmanowi.
     * @param secondHetman Kolumna odpowiadająca 2. giemu hetmanowi.
     * @param state Stan zawierający ustawienie hetmanów na szachownicy.
     */
    private void flipHetmans(int firstHetman, int secondHetman, int[] state){
        int firstHetmanRow = state[firstHetman];
        int secondHetmanRow = state[secondHetman];

        state[firstHetman] = secondHetmanRow;
        state[secondHetman] = firstHetmanRow;
    }

    /**
     * Generuje losowy stan hetmanów na szachownicy.
     */
    private void generateRandomState(){
        if(actualState == null)
            actualState = new int[size];
        var set = PermutationGenerator.getPermutationSet(size, 0, size-1);

        for (int i = 0; i < size; i++) {
            actualState[i] = set.get(i);
        }
    }

    /**
     * Zlicza liczbę konfliktów dka wprowadzonego stanu.
     * @param state Sprawdzany stan.
     * @return Liczba konfliktów dla stanu.
     */
    public int getConflictsCount(int[] state){
        int conflicts = 0;

        for (int column = 0; column < size; column++) {
            //Sprawdzanie dla wiersza
            var actualHetmanRow = state[column];//wiersz, w którym znajduje się hetman z columny
            int progress = 0;
            for (int nextColumn = column + 1; nextColumn < size; nextColumn++) {
                progress+=1;
                var nextHetmanRow = state[nextColumn];
                if(nextHetmanRow == actualHetmanRow)//konflikt w wierszu
                    conflicts++;
                else if(nextHetmanRow == (actualHetmanRow + progress) && ((actualHetmanRow + progress) < size))//konflikt po skosie w dół
                    conflicts++;
                else if(nextHetmanRow == (actualHetmanRow - progress) && ((actualHetmanRow - progress) >= 0))//konflikt po skosie w górę
                    conflicts++;
            }
        }

        return conflicts;
    }

    /**
     * Ustawia hetmana na wskazanej pozycji.
     * @param state Stan, dla którego będzie ustawiany hetman.
     * @param row Wiersz, w którym znajdować się ma docelowo hetman.
     * @param column Kolumna, w której znajdować się ma docelowo hetman.
     */
    public void setHetmanOnPosition(int[] state, int row, int column){
        state[column] = row;
    }

    /**
     * W przystępny sposób wyświetla rozwiązanie.
     */
    public void showResult(){
        if (solved) {
            System.out.println(this);
        }else {
            System.out.println("Problem n hetmanów nie jest (jeszcze) rozwiązany!");
        }
    }

    @Override
    public String toString() {
        //Zwraca String prezentujący aktualny stan szachownicy
        StringBuilder builder = new StringBuilder();

        for (int row = 0; row < size; row++) {
            builder.append("| ");
            for (int column = 0; column < size; column++){
                if(actualState[column] == row)
                    builder.append("X");
                else
                    builder.append(" ");
                builder.append(" | ");
            }
            builder.append("\n");
        }

        return builder.toString();
    }
}