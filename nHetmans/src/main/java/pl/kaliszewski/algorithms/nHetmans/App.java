package pl.kaliszewski.algorithms.nHetmans;

import java.util.Scanner;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Proszę podać wymiar szachownicy: " );
        Scanner input = new Scanner(System.in);

        try{
            byte size = Byte.parseByte(input.nextLine());
            nHetmans problem = new nHetmans(size);
            problem.prepareSolution();
        }catch (Exception e){
            System.out.println("Przechwycono wyjątek: \n" + e.getMessage());
        }
    }
}
