package pl.kaliszewski.algorithms.HashTable;

import java.lang.reflect.Modifier;

/**
 * Implementacja haszowania z mechanizmem dynamicznej tablicy.
 * Mechanizm obsługuje typy dla:
 * <ul>
 *     <li>Integer,</li>
 *     <li>Long,</li>
 *     <li>Double,</li>
 *     <li>String,</li>
 *     <li>proste klasy z kilkoma polami o typach wymienionych pól wymienionych wcześniej.</li>
 * </ul>
 * <b>Dodanie istniejącego już elementu jest pomijane.</b>
 */
public class HashTable<Type> {
    /**
     * Domyślny, startowy rozmiar tablicy przechowującej elementy.
     */
    private int DEFAULT_SIZE = 13;
    /**
     * Aktualny rozmiar tablicy przechowującej elementy.
     */
    private long actualSize;
    /**
     * Liczba elementów umieszczonych w strukturze.
     */
    private int elementsCount;
    /**
     * Flaga określająca czy tablica przechowująca elementy ma mieć zmieniany rozmiar stosownie do jej "zajętości".
     */
    private boolean noResize = false;
    /**
     * Wskaźnik służący do pomniejszania tablicy zawierającej elementy.
     */
    private final static float DECREASE = 0.4F;
    /**
     * Wskaźnik służący do powiększania tablicy zawierającej elementy.
     */
    private static float EXTEND = 1.6F;
    /**
     * Tablica zawierająca elementy.
     * Tablica przechowuje:
     * <ul>
     *     <li>Wprowadzoną wartość,</li>
     *     <li>Puste miejsce: <i>null</i>,</li>
     *     <li>Miejsce puste po elemencie usuniętym: Object</li>
     * </ul>
     */
    private Object[] baseTable;

    /**
     * Pole przechowywujące informację o klasie umieszczanych elementów.
     */
    private Class<?> clazz;

    /**
     * Konstruktor tablicy haszującej.
     * @param size Początkowy rozmiar tablicy przechowującej elementy.
     * @param dummy Zawiera informację o klasie przechowywanych elementów.
     */
    public HashTable(int size, Type... dummy) {
        DEFAULT_SIZE = size;
        Init(dummy);
    }

    /**
     * Konstruktor tablicy haszującej.
     * @param size Początkowy rozmiar tablicy przechowującej elementy.
     * @param noResize Flaga oznaczająca czy rozmiar tablicy ma być statyczny. Pomijanie jej powiększania w trakcie używania.
     * @param dummy Zawiera informację o klasie przechowywanych elementów.
     *
     */
    public HashTable(int size, boolean noResize, Type... dummy) {
        DEFAULT_SIZE = size;
        this.noResize = noResize;
        Init(dummy);
    }

    /**
     * Konstruktor tablicy haszującej.
     * @param size Początkowy rozmiar tablicy przechowującej elementy.
     * @param noResize Flaga oznaczająca czy rozmiar tablicy ma być statyczny. Pomijanie jej powiększania w trakcie używania.
     * @param extend Współczynnik o jaki powiększamy rozmiar tablicy haszującej.
     * @param dummy Zawiera informację o klasie przechowywanych elementów.
     */
    public HashTable(int size, boolean noResize, float extend, Type... dummy) {
        EXTEND = extend;
        DEFAULT_SIZE = size;
        this.noResize = noResize;
        Init(dummy);
    }

    /**
     * Domyślny konstruktor tablicy haszującej.
     * @param dummy Zawiera informację o klasie przechowywanych elementów.
     */
    public HashTable(Type... dummy) {
        Init(dummy);
    }

    private void Init(Type... dummy){
        if (dummy.length > 0)
            throw new IllegalArgumentException("Błędny argument atrapy!");
        this.clazz = dummy.getClass().getComponentType();
        baseTable = new Object[DEFAULT_SIZE];
        actualSize = DEFAULT_SIZE;
    }

    /**
     * Dodaje pojedynczy element do tablicy haszującej.
     * @param element Element do dodania.
     * @throws Exception Wyjątek otrzymywany podczas błędu otrzymania hasha dla elementu.
     * Wyjątek rzucany także w przypadku nie dodania elementu.
     */
    public int addElement(Type element) throws Exception {
        int hashCode;
        int attempt = 0;

        try {
             hashCode = getHashCode(element);
        } catch (IllegalAccessException e) {
            String error = String.format("Błąd podczas otrzymywania hasza dla elementu: %s\n", e.getMessage());
            System.out.println(error);
            throw new Exception(error);
        }

        while(attempt < baseTable.length){
            int index = getIndex(hashCode, attempt);

            if (baseTable[index] == null){//puste
                //dodanie elementu
                elementsCount++;
                baseTable[index] = element;
                System.out.printf("Dodano element %s do zbioru!\n", element);
                if(!noResize)
                    resizeBaseTable();
                return attempt;
            }
            try {
                int indexElementHashCode = getHashCode((Type)baseTable[index]);
                if(indexElementHashCode == hashCode){
                    String error = String.format("Element %s istnieje już w zbiorze!\n", element);
                    throw new Exception(error);
                }else{
                    attempt++;//lokalizacja już zajęta, podjęta będzie kolejna próba.
                }
            }catch (ClassCastException | IllegalAccessException exception){//puste, lecz coś było
                //dodanie elementu
                elementsCount++;
                baseTable[index] = element;
                System.out.printf("Dodano element %s do zbioru!\n", element);
                if(!noResize)
                    resizeBaseTable();
                return attempt;
            }
        }

        System.err.printf("Nie dodano elementu %s do zbioru!\n", element);
        throw new Exception(String.format("Nie dodano elementu %s do zbioru!\n", element));
    }

    /**
     * Usuwa wskazany element ze struktury tablicy haszującej.
     * Porównanie elementów i usunięcie polega na wyznaczeniu hasha oraz porównaniu hasha z elementem na wyznaczonej pozycji w tablicy haszującej.
     * @param element Element do usunięcia.
     */
    public void removeElement(Type element){
        int hashCode;
        int attempt = 0;

        try {
            hashCode = getHashCode(element);
        } catch (IllegalAccessException e) {
            System.out.println(String.format("Błąd podczas otrzymywania hasza dla elementu: %s\n", e.getMessage()));
            return;
        }

        /*Faktyczne usunięcie z tablicy*/
        while(attempt < baseTable.length){
            int index = getIndex(hashCode, attempt);

            try {
                if (baseTable[index] == null){
                    System.out.printf("Element %s nie istnieje!\n", element);
                    return;
                }

                int indexElementHashCode = getHashCode((Type)baseTable[index]);
                if(indexElementHashCode == hashCode){
                    baseTable[index] = new Object();
                    actualSize--;
                    if (!noResize)
                        resizeBaseTable();
                    System.out.printf("Usunięto element %s\n", element);
                }else{
                    attempt++;
                }
            } catch (Exception e) {
                System.out.println(String.format("Błąd podczas otrzymywania hasza dla elementu: %s\n", e.getMessage()));
            }
        }
    }

    /**
     * Wyszukuje wskazany element w tablicy haszującej.
     * @param element Szukany element w tablicy haszującej.
     * @return Zwraca podsumowanie poszukiwania elementu.
     */
    public Result touchElement(Type element){
        int steps = 1;
        int attempt = 0;
        int hashCode;

        try {
            hashCode = getHashCode(element);
        } catch (IllegalAccessException e) {
            System.out.println(String.format("Błąd podczas otrzymywania hasza dla elementu: %s\n", e.getMessage()));
            return new Result(0, false);
        }

        while(attempt < baseTable.length){
            int index = getIndex(hashCode, attempt);

            try {
                if (baseTable[index] == null){
                    System.err.printf("Element %s nie został odnaleziony w %d krokach!\n", element, steps);
                    return new Result(steps, false);
                }

                int indexElementHashCode = getHashCode((Type)baseTable[index]);
                if(indexElementHashCode == hashCode){
                    return new Result(steps, true);
                }else{
                    attempt++;
                    steps++;
                }
            } catch (Exception e) {
                System.out.println(String.format("Błąd podczas otrzymywania hasza dla elementu: %s\n", e.getMessage()));
            }
        }

        return new Result(steps, false);
    }

    private void resizeBaseTable() throws Exception {
        double ratio = getDataRatio();
        if (ratio >= 0.85)
            extendBaseTable();
        if(ratio <= 0.30 && actualSize != DEFAULT_SIZE)
            decreaseBaseTable();
    }

    private void decreaseBaseTable() throws Exception {
        modifyBaseTableSize(DECREASE);
    }

    private void extendBaseTable() throws Exception {
        modifyBaseTableSize(EXTEND);
    }

    private void modifyBaseTableSize(double factor) throws Exception {
        long newSize = Math.round(actualSize * factor);
        int newSizeInt = Math.toIntExact(newSize);
        Object[] newTable = new Object[newSizeInt];

        for (var item : baseTable) {
            if (item != null) {
                try {
                    int elementHashCode = getHashCode((Type) item);
                    int newIndex = getIndex(elementHashCode, newSizeInt);
                    newTable[newIndex] = item;
                } catch (Exception e) {
                    String error = String.format("Błąd podczas modyfikacji rozmiaru tablicy dla elementu: %s\n", e.getMessage());
                    System.out.println(error);
                    throw new Exception(error);
                }
            }
        }

        baseTable = newTable;
        actualSize = newSize;
    }

    /**
     * Uzyskuje stosunek zajętości tablicy haszującej.
     * @return Stosunek zajętości tablicy haszującej.
     */
    private double getDataRatio(){
        return (double) elementsCount / actualSize;
    }

    /**
     * Wyznacza hasz dla wskazanego obiektu.
     * @param objectToAdd Obiekt, którego wyznaczany jest hasz.
     * @return Wyznaczony kod hasz.
     * @throws IllegalAccessException
     */
    private int getHashCode(Type objectToAdd) throws IllegalAccessException {
        int sum = 0;

        if (clazz == Integer.class){
            return (int) objectToAdd;
        }
        if (clazz == Double.class){
            return getDoubleHashCode((double) objectToAdd);
        }
        if (clazz == Long.class){
            return (int) objectToAdd;
        }
        if (clazz == String.class){
            return getStringHashCode((String) objectToAdd);
        }
        if (clazz == Character.class){
            return (int) objectToAdd;
        }

        //dla klas
        var fields = clazz.getDeclaredFields();
        for (var field : fields) {
            if(Modifier.isPrivate(field.getModifiers()) || Modifier.isProtected(field.getModifiers()) || Modifier.isPublic(field.getModifiers())){
                Class<?> fieldClass = field.getType();
                field.setAccessible(true);
                Object value = field.get(objectToAdd);

                if (fieldClass == Integer.class){
                    sum += (Integer) value;
                }
                if (fieldClass == Double.class){
                    sum += getDoubleHashCode((double)objectToAdd);
                }
                if (fieldClass == Long.class){
                    sum += (Long) value;
                }
                if (fieldClass == String.class){
                    sum += getStringHashCode((String) value);
                }
                if (clazz == Character.class){
                    sum += (Integer) objectToAdd;
                }
            }
        }

        return sum;
    }

    /**
     * Wyznacza indeks pod którym ma zostać umieszczony element w tablicy haszującej.
     * @param hashCode Hasz dla obiektu szukanego bądź dodawanego.
     * @param attempt Numer podejmowanyej próby.
     * @return Numer indeksu.
     */
    private int getIndex(int hashCode, int attempt){
        int hash1 = Math.abs(hashCode % (int) actualSize);
        int hash2 = 1 + Math.abs(hashCode % (int)(actualSize -1));
        int result = Math.abs((hash1 + attempt * hash2) % (int) actualSize);

        return result;
    }

    /**
     * Zwraca wartość hasza dla obiektu typu Double.
     * @param value  Obiekt typu Double.
     * @return Wartość hasza dla wartości value
     */
    private int getDoubleHashCode(double value){
        long bits = Double.doubleToRawLongBits(value); //wartość bitowa wg IEEE_754
        return (int) (bits ^ (bits >>> 32)); //Wykonanie operacji XOR na oryginalnej wartości z połową bitów (pierwsze 32).
    }

    /**
     * Zwraca wartość hasza dla obiektu typu String.
     * Wykorzystywany algorytm haszowania łańcucha znakowego to <b>djb2</b>
     * @param value Obiekt typu String.
     * @return Wartość hasza dla wartości value.
     */
    private int getStringHashCode(String value){
        long hashCode = 5381;
        for (int i = 0; i < value.length() - 1; i++) {
            char character = value.charAt(i);
            hashCode =((hashCode << 5) + hashCode) + Character.hashCode(character);
        }

        return (int) hashCode;
    }

    /**
     * Zwraca liczbę elementów. Na podobieństwo tego jak to robi ArrayList
     * @return Liczba elementów w tablicy haszującej.
     */
    public int size(){
        return elementsCount;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("HashTable - Typ danych: ").append(clazz.getTypeName()).append("\n");
        builder.append("Rozmiar: ").append(actualSize).append("\n");
        builder.append("Liczba elementów: ").append(elementsCount).append("\n");
        builder.append("Elementy w Hashtabeli:\n");

        int index = 0;
        for (var record : baseTable) {
            if (record == null) {
                builder.append(String.format("[%3d]: Puste,\n", index));
                index++;
                continue;
            }

            try{
                Type obj = (Type) record;
                builder.append(String.format("[%3d]: %s,\n", index, obj));
                index++;
            } catch(Exception e){
                builder.append(String.format("[%3d]: Puste, po elemencie usuniętym,\n", index));
                index++;
            }
        }

        return builder.toString();
    }

    /**
     * Rezulat poszukiwania wskazanego elementu w tablicy haszującej.
     */
    class Result{
        /**
         * Liczba kroków podjętych w celu osiągnięcia rezultatu.
         */
        private int steps;

        /**
         * Flaga czy poszukiwany element został odnalezniony
         */
        private boolean found;

        private Result(int steps, boolean found) {
            this.steps = steps;
            this.found = found;
        }

        public int getSteps() {
            return steps;
        }

        public boolean isFound() {
            return found;
        }
    }
}
