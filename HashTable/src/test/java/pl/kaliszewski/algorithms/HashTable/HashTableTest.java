package pl.kaliszewski.algorithms.HashTable;

import IO.File;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class HashTableTest {

    void stringClassTest(int elements, int initSize){
        Dataset dataset = null;
        try {
            dataset = createDataset("src/main/resources/nazwiska.txt", elements);
        } catch (Exception e){
            Assertions.fail();
        }

        List<String> createSurnames = dataset.createItems;
        List<String> testSurnames = dataset.testItems;

        HashTable<String> set = new HashTable<String>(initSize, true);
        for (var surname : createSurnames) {
            try{
                set.addElement(surname);
            }catch (Exception e){
                System.out.println(e);
            }
        }

        System.out.println(set);

        double findAverage = 0;
        int findResult = 0;
        double notFindAverage = 0;
        int notFindResult = 0;

        for(var surname : testSurnames){
            HashTable.Result result = set.touchElement(surname);
            if(result.isFound()){
                System.out.printf("%s znaleziono w %d krokach!\n", surname, result.getSteps());
                findResult++;
                findAverage += result.getSteps();
            }else{
                if(createSurnames.contains(surname)){
                    System.err.printf("Nie znaleziono %s, mimo, że został dodany!\n", surname);
                    Assertions.fail();
                }

                notFindResult++;
                notFindAverage += result.getSteps();
            }
        }
        //System.out.printf("Odnaleziono %d elementów na %d!\n", result, testSurnames.size());
        findAverage = findAverage / findResult;
        notFindAverage = notFindAverage / notFindResult;
        System.out.printf("Średnio wykonano %f kroków by odnaleźć rekord!\n", findAverage);
        System.out.printf("Średnio wykonano %f kroków, gdy nie odnaleziono rekordu!\n", notFindAverage);
    }

    void ownClassTest(int elements, int initSize, boolean noResize){
        Dataset dataset = null;
        try {
            dataset = createDataset("src/main/resources/nazwiska.txt", elements);
        } catch (Exception e){
            Assertions.fail();
        }

        List<Person> createPersons = new ArrayList<>(elements);
        var createItems = dataset.createItems;
        for (int i = 0; i < createItems.size(); i++) {
            createPersons.add(new Person(createItems.get(i), i));
        }

        List<Person> testPersons = new ArrayList<>(elements);
        var testItems = dataset.testItems;
        for (int i = 0; i < testItems.size(); i++) {
            var actualItem = testItems.get(i);
            var stream = createPersons.stream().filter(item -> item.surnname.equals(actualItem)).findAny();
            if(stream.isEmpty())
                testPersons.add(new Person(testItems.get(i), i));
            else
                testPersons.add(stream.get());
        }

        HashTable<Person> set = new HashTable<Person>(initSize, noResize);
        for (var person : createPersons) {
            try{
                set.addElement(person);
            }catch (Exception e){
                System.out.println(e);
            }
        }
        System.out.println(set);

        double findAverage = 0;
        int findResult = 0;
        double notFindAverage = 0;
        int notFindResult = 0;

        for(var person : testPersons){
            HashTable.Result result = set.touchElement(person);
            if(result.isFound()){
                System.out.printf("%s znaleziono w %d krokach!\n", person, result.getSteps());
                findResult++;
                findAverage += result.getSteps();
            }else if(!result.isFound()){
                if(createPersons.contains(person)){
                    System.err.printf("Nie znaleziono %s, mimo, że został dodany!\n", person);
                    Assertions.fail();
                }

                notFindResult++;
                notFindAverage += result.getSteps();
            }
        }
        //System.out.printf("Odnaleziono %d elementów na %d!\n", result, testSurnames.size());
        findAverage = findAverage / findResult;
        notFindAverage = notFindAverage / notFindResult;
        System.out.printf("Średnio wykonano %f kroków by odnaleźć rekord!\n", findAverage);
        System.out.printf("Średnio wykonano %f kroków, gdy nie odnaleziono rekordu!\n", notFindAverage);
    }

    void integerTestWithoutDatasetTest(int elements, int initSize, float extend, boolean noResize){
        HashTable<Integer> set = new HashTable<Integer>(initSize, noResize, extend);
        try(var stream = new FileOutputStream(new java.io.File("result_"+extend+".csv"))){
            for (int i = 0; i < elements; i++) {
                try{
                    var element = (int) (Math.random() * 10 * elements);
                    var attempts = set.addElement(element) +1;
                    String content = element +";" + attempts+"\n";
                    stream.write(content.getBytes());
                    System.out.printf("Liczba prób podjętych do umieszczenia elementu: %d wynosi: %d\n", element, attempts);
                }catch (Exception e){
                    System.out.println(e);
                }
            }
        }catch(Exception fileException){
            System.out.println(fileException.getMessage());
        }
    }

    @Test
    void stringTest_70_Elements_200_Size(){
        int elements = 70;
        stringClassTest(elements, 200);
    }

    @Test
    void stringTest_100_Elements_200_Size(){
        int elements = 100;
        stringClassTest(elements, 200);
    }

    @Test
    void stringTest_130_Elements_200_Size(){
        int elements = 130;
        stringClassTest(elements, 200);
    }

    @Test
    void stringTest_150_Elements_200_Size(){
        int elements = 150;
        stringClassTest(elements, 200);
    }

    @Test
    void stringTest_185_Elements_200_Size(){
        int elements = 185;
        stringClassTest(elements, 200);
    }

    @Test
    void stringTest_185_Elements_400_Size(){
        int elements = 185;
        stringClassTest(elements, 400);
    }

    @Test
    void ownClassTest_70_Elements_200_Size(){
        int elements = 70;
        ownClassTest(elements, 200, true);
    }

    @Test
    void ownClassTest_100_Elements_200_Size(){
        int elements = 100;
        ownClassTest(elements, 200, true);
    }

    @Test
    void ownClassTest_130_Elements_200_Size(){
        int elements = 130;
        ownClassTest(elements, 200, true);
    }

    @Test
    void ownClassTest_150_Elements_200_Size(){
        int elements = 150;
        ownClassTest(elements, 200, true);
    }

    @Test
    void ownClassTest_185_Elements_200_Size(){
        int elements = 185;
        ownClassTest(elements, 200, true);
    }

    @Test
    void ownClassTest_185_Elements_400_Size(){
        int elements = 185;
        ownClassTest(elements, 400, true);
    }


    @Test
    void ownClassTest_185_Elements_Default_Size(){
        int elements = 185;
        ownClassTest(elements, 13, false);
    }

    @Test
    void integerTest_1000000_Elements_Default_SizeExtend1_6(){
        int elements = 1000000;
        integerTestWithoutDatasetTest(elements, 13, 1.6f, false);
    }
    @Test
    void integerTest_1000000_Elements_Default_SizeExtend2_0(){
        int elements = 1000000;
        integerTestWithoutDatasetTest(elements, 13, 2.f, false);
    }
    Dataset createDataset(String filename, int size) throws Exception {
        List<String> items = File.loadDataFromFile(filename);
        Random random = new Random();

        List<String> createItems = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            while(true){
                int index = random.nextInt(items.size());
                var choose = items.get(index);
                if(!createItems.contains(choose)){
                    createItems.add(choose);
                    break;
                }
            }
        }

        List<String> testItems = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            if(i % 2 == 0){
                while(true){
                    int index = random.nextInt(createItems.size());
                    var choose = createItems.get(index);
                    if(!testItems.contains(choose)){
                        testItems.add(choose);
                        break;
                    }
                }
            }else{
                while(true){
                    int index = random.nextInt(items.size());
                    var choose = items.get(index);
                    if(!createItems.contains(choose)){
                        testItems.add(choose);
                        break;
                    }
                }
            }
        }

        return new Dataset(testItems, createItems);
    }

    class Dataset{
        List<String> testItems;
        List<String> createItems;

        public Dataset(List<String> testItems, List<String> createItems) {
            this.testItems = testItems;
            this.createItems = createItems;
        }
    }

    public class Person{
        private final String surnname;
        private final Integer age;

        public Person(String surnname, Integer age) {
            this.surnname = surnname;
            this.age = age;
        }

        public String getSurnname() {
            return surnname;
        }

        public Integer getAge() {
            return age;
        }

        @Override
        public String toString() {
            return "Nazwisko: " + surnname + ", " +
                    "Wiek: " + age;
        }
    }
}
