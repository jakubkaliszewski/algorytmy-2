package pl.kaliszewski;

import Graph.Graph;
import com.brunomnsilva.smartgraph.containers.SmartGraphDemoContainer;
import com.brunomnsilva.smartgraph.graph.DigraphEdgeList;
import com.brunomnsilva.smartgraph.graphview.SmartCircularSortedPlacementStrategy;
import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel;
import com.brunomnsilva.smartgraph.graphview.SmartPlacementStrategy;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class GraphVisualization {
    private com.brunomnsilva.smartgraph.graph.Graph<String, String> viewGraph;
    private SmartGraphPanel<String, String> view;

    public void createVisualization(Graph graph, String title) {
        viewGraph = buildGraph(graph);
        //Graph<String, String> viewGraph = build_flower_graph();
        System.out.println(viewGraph);

        SmartPlacementStrategy strategy = new SmartCircularSortedPlacementStrategy();
        //SmartPlacementStrategy strategy = new SmartRandomPlacementStrategy();
        view = new SmartGraphPanel<>(viewGraph, strategy);

        /*
        Basic usage:
        Use SmartGraphDemoContainer if you want zoom capabilities and automatic layout toggling
        */
        //Scene scene = new Scene(graphView, 1024, 768);
        Scene scene = new Scene(new SmartGraphDemoContainer(view), 1024, 768);

        Stage stage = new Stage(StageStyle.DECORATED);
        stage.setTitle(title);
        stage.setMinHeight(600);
        stage.setMinWidth(800);
        stage.setScene(scene);
        stage.show();

        /*
        IMPORTANT: Must call init() after scene is displayed so we can have width and height values
        to initially place the vertices according to the placement strategy
        */
        view.init();

        /*
        Bellow you can see how to attach actions for when vertices and edges are double clicked
         */
        //graphView.setVertexDoubleClickAction(graphVertex -> {
        //    System.out.println("Vertex contains element: " + graphVertex.getUnderlyingVertex().element());

            //want fun? uncomment below with automatic layout
            //viewGraph.removeVertex(graphVertex.getUnderlyingVertex());
            //graphView.update();
        //});

        //graphView.setEdgeDoubleClickAction(graphEdge -> {
        //    System.out.println("Edge contains element: " + graphEdge.getUnderlyingEdge().element());
            //dynamically change the style when clicked
        //    graphEdge.setStyle("-fx-stroke: black; -fx-stroke-width: 2;");

            //uncomment to see edges being removed after click
            //Edge<String, String> underlyingEdge = graphEdge.getUnderlyingEdge();
            //viewGraph.removeEdge(underlyingEdge);
            //graphView.update();
        //});
    }

    private static com.brunomnsilva.smartgraph.graph.Graph<String, String> buildGraph(Graph graph) {

        com.brunomnsilva.smartgraph.graph.Graph<String, String> graphFX = new DigraphEdgeList<>();
        graph.getNodesId().forEach(node -> {
            graphFX.insertVertex(String.valueOf(node));
        });

        graph.getEdges().forEach(edge -> {
            graphFX.insertEdge(edge.getFirstNodeName(), edge.getSecondNodeName(), edge.toString());
        });

        return graphFX;
    }
}
