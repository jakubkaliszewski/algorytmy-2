package pl.kaliszewski.algorithms.CNF2;

import java.util.Map;

public class CNF2Result {
    private String formula;
    private Map<String, Boolean> variablesStates;

    public CNF2Result(Map<String, Boolean> variablesStates, String formula) {
        this.variablesStates = variablesStates;
        this.formula = formula;
    }

    public Boolean getStateForVariable(String variable) throws Exception {
        try{
            return variablesStates.get(variable);
        }catch (Exception e){
            throw new Exception("Podana zmienna nie istnieje!");
        }
    }

    @Override
    public String toString() {
        return "Rezultat CNF:\n" +
                "Formuła: " + this.formula +"\n" +
                "Wartości: \n" + variablesStates.toString();
    }
}
