package pl.kaliszewski.algorithms.CNF2;

import Graph.Graph;

import java.util.LinkedHashMap;
import java.util.Map;

public class ImplicationGraph extends Graph {

    private Map<String, Integer> nodes;

    public ImplicationGraph() {
        super(10);
        nodes = new LinkedHashMap<>();
    }

    public void addNode(String nodeName) throws Exception {
        if(isNodeExist(nodeName))
            throw new Exception(String.format("Wierzchołek o nazwie %s już istnieje!", nodeName));

        int index = super.addNode();
        nodes.put(nodeName, index);
    }

    public void addEdge(String nodeName1, String nodeName2) throws Exception {
        if(!isNodeExist(nodeName1) || !isNodeExist(nodeName2))
            throw new Exception("Jeden z podanych wierzchołków nie istnieje w grafie!");

        int node1Index = nodes.get(nodeName1);
        int node2Index = nodes.get(nodeName2);

        super.addNeighborForNode(node1Index, node2Index, 1, false);
    }

    private boolean isNodeExist(String nodeName){
        return nodes.containsKey(nodeName);
    }

    public Map<String, Integer> getNodes() {
        return nodes;
    }
}
