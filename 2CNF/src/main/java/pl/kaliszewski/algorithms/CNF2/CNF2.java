package pl.kaliszewski.algorithms.CNF2;

import Graph.StronglyConnectedComponent;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CNF2 {
    static public CNF2Result solveProblem(String formula) throws Exception {
        ImplicationGraph graph = ImplicationGraphFactory.createImplicationGraph(formula);
        var SCC = StronglyConnectedComponent.getStronglyConnectedComponents(graph);

        var nodesNameIndex = graph.getNodes();
        var nodesIndexName = graph.getNodes().entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
        Map<Integer, Boolean> nodesIndexState = new HashMap<>(graph.getNodesCount());

        for(var strongComponentKey : SCC.keySet()){
            List<Integer> strongComponent = SCC.get(strongComponentKey);
            for (var node : strongComponent) {
                if (nodesIndexState.containsKey(node)) {
                    continue;
                }

                String nodeName = nodesIndexName.get(node);
                String negationNodeName = getNegation(nodeName);
                int negationIndex = nodesNameIndex.get(negationNodeName);
                if(strongComponent.contains(negationIndex))
                    throw new Exception("Podana formuła jest niespełnialna!");

                nodesIndexState.put(node, false);
                nodesIndexState.put(negationIndex, true);
            }
        }

        CNF2Result result = buildResult(nodesIndexName, nodesIndexState, formula);
        //sprawdzić czy formuła ma wynik true
        if(!formulaIsTrue(formula, result))
            throw new Exception("Osiągnięte rozwiązanie formuły jest nieprawidłowe! Zaproponowane rozwiązanie: \n" + result);

        return result;
    }

    private static CNF2Result buildResult(Map<Integer, String> nodesIndexName, Map<Integer, Boolean> nodesIndexState, String formula) {
        Map<String, Boolean> map = new LinkedHashMap<>(nodesIndexName.size());
        nodesIndexName.forEach((key, value) -> {
            var state = nodesIndexState.get(key);
            map.put(value, state);
        });
        return new CNF2Result(map, formula);
    }

    static private String getNegation(String nodeName){
        if(nodeName.startsWith("~"))
            return nodeName.substring(1);
        else
            return "~" + nodeName;
    }


    static private Boolean formulaIsTrue(String formula, CNF2Result cnf2Result) throws Exception {
        //wyznacz wartość każdego nawiasu, wszystkie muszą być na true!
        var brackets = formula.split("\\^");
        for (var bracket : brackets) {
            var trimBracket = bracket.trim();
            if (!trimBracket.startsWith("(") || !trimBracket.endsWith(")"))
                throw new Exception("Formuła logiczna posiada nie sparowane nawiasy!");

            var variables = trimBracket.split("v");
            if (variables.length != 2)
                throw new Exception("Formuła logiczna nie jest formułą 2CNF!");

            var variable1 = variables[0].substring(1).trim();
            var variable2 = variables[1].substring(1, variables[1].length() - 1);

            boolean state1 = cnf2Result.getStateForVariable(variable1);
            boolean state2 = cnf2Result.getStateForVariable(variable2);

            if(!state1 && !state2)
                return false;
        }

        return true;
    }
}
