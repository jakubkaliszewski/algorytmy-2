package pl.kaliszewski.algorithms.CNF2;

import java.util.Arrays;

public class ImplicationGraphFactory {

    public static ImplicationGraph createImplicationGraph(String formula) throws Exception {
        ImplicationGraph graph = new ImplicationGraph();

        try {
            var brackets = formula.split("\\^");
            for (var bracket : brackets) {
                var trimBracket = bracket.trim();
                if (!trimBracket.startsWith("(") || !trimBracket.endsWith(")"))
                    throw new Exception("Formuła logiczna posiada nie sparowane nawiasy!");

                var variables = trimBracket.split("v");
                if (variables.length != 2)
                    throw new Exception("Formuła logiczna nie jest formułą 2CNF!");

                var variable1 = variables[0].substring(1).trim();
                var variable2 = variables[1].substring(1, variables[1].length() - 1);

                var variable1NOT = variable1.startsWith("~");
                var variable2NOT = variable2.startsWith("~");

                if (variable1NOT) {
                    if (variable2NOT) {
                        /*(~P1 v ~P2)*/
                        var NOTVariable1 = variable1;
                        var NOTVariable2 = variable2;
                        variable1 = variable1.substring(1);
                        variable2 = variable2.substring(1);

                        addNodes(
                                graph,
                                NOTVariable1,
                                NOTVariable2,
                                variable1,
                                variable2
                        );

                        graph.addEdge(variable1, NOTVariable2);
                        graph.addEdge(variable2, NOTVariable1);
                    } else {
                        /*(~P1 v ~P2)*/
                        var NOTVariable1 = variable1;
                        var NOTVariable2 = "~" + variable2;
                        variable1 = variable1.substring(1);

                        addNodes(
                                graph,
                                NOTVariable1,
                                NOTVariable2,
                                variable1,
                                variable2
                        );

                        graph.addEdge(variable1, variable2);
                        graph.addEdge(NOTVariable2, NOTVariable1);
                    }
                } else {
                    if (variable2NOT) {
                        /*(P1 v ~P2)*/
                        var NOTVariable1 = "~" + variable1;
                        var NOTVariable2 = variable2;
                        variable2 = variable2.substring(1);

                        addNodes(
                                graph,
                                NOTVariable1,
                                NOTVariable2,
                                variable1,
                                variable2
                        );

                        graph.addEdge(NOTVariable1, NOTVariable2);
                        graph.addEdge(variable2, variable1);
                    } else {
                        /*(P1 v P2)*/
                        var NOTVariable1 = "~" + variable1;
                        var NOTVariable2 = "~" + variable2;

                        addNodes(
                                graph,
                                NOTVariable1,
                                NOTVariable2,
                                variable1,
                                variable2
                        );

                        graph.addEdge(NOTVariable1, variable2);
                        graph.addEdge(NOTVariable2, variable1);
                    }
                }
            }
        } catch (Exception e) {
            var message = "Wystąpił błąd przy przetwarzaniu formuły logicznej!\n" + e.getMessage();
            throw new Exception(message);
        }

        return graph;
    }

    private static void addNodes(ImplicationGraph graph, String node1, String node2, String node3, String node4) {
        var list = Arrays.asList(node1, node2, node3, node4);
        for (var node : list) {
            try {
                graph.addNode(node);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
