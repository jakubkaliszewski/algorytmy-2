package pl.kaliszewski.algorithms.CNF2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CNF2Test {

    @Test
    void NoSAT2Problem() {
        String formula = "(x1 v x2) ^ (x1 v ~x2) ^ (~x1 v x2) ^ (~x1 v ~x2)";
        try {
            var result = CNF2.solveProblem(formula);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.assertTrue(true);
        }
    }

    @Test
    void SAT2Problem_0() {
        String formula = "(P1 v P2) ^ (~P2 v ~P3) ^ (~P1 v P3) ^ (P3 v ~P2)";
        try {
            var result = CNF2.solveProblem(formula);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    void SAT2Problem_1(){
        String formula = "(a v ~b) ^ (~a v b) ^ (~a v ~b) ^ (a v ~c)";
        try {
            var result = CNF2.solveProblem(formula);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    void SAT2Problem_2(){
        String formula = "(x0 v x1) ^ (x1 v ~x2) ^ (~x1 v x0) ^ (x2 v ~x3) ^ (x0 v x3) ^ (~x0 v ~x1) ^ (x3 v ~x2)";
        try {
            var result = CNF2.solveProblem(formula);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    void SAT2Problem_3(){
        String formula = "(P1 v ~P2) ^ (~P1 v ~P2) ^ (~P1 v ~P2) ^ (P1 v ~P3)";
        try {
            var result = CNF2.solveProblem(formula);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }
}
