package pl.kaliszewski.algorithms.CNF2;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ImplicationGraphFactoryTest {
    @Test
    void createImplicationGraph(){
        String formula = "(P1 v P2) ^ (~P2 v ~P3) ^ (~P1 v P3) ^ (P3 v ~P2)";
        try {
            var graph = ImplicationGraphFactory.createImplicationGraph(formula);
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }
}
