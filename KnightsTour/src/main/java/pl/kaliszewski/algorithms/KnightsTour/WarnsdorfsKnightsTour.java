package pl.kaliszewski.algorithms.KnightsTour;

import java.util.PriorityQueue;
import java.util.Random;

/**
 * Klasa rozwiązująca Problem skoczka szachowego metodą Warnsdorfa.
 */
public class WarnsdorfsKnightsTour {
    /**
     * Stan odwiedzonych pól na szachownicy przez skoczka.
     * Rozwiązanie.
     */
    private int[][] state;
    /**
     * Określa liczbę dostępnych ruchów z pola o identycznych indeksach jak ten wpis.
     */
    private int[][] availableMoves;
    /**
     * Określa liczbę kolumn oraz wierszy na szachownicy.
     */
    private int size;
    /**
     * Określa czy rozwiązano problem.
     */
    private boolean solved;
    /**
     * Określa liczbę pól na szachownicy.
     */
    private int fields;
    /**
     * Stanowi możliwe zmiany kolumny w następnym ruchu skoczka.
     */
    final int columnMove[] = {2, 1, -1, -2, -2, -1, 1, 2};
    /**
     * Stanowi możliwe zmiany kolumny w następnym ruchu skoczka.
     */
    final int rowMove[] = {1, 2, 2, 1, -1, -2, -2, -1};

    public WarnsdorfsKnightsTour(int size) {
        this.size = size + 4;
        this.fields = size * size;
    }

    private int[][] prepareAvailableMoves(){
        int size = this.size -4;
        var availableMoves = new int[size][size];//przy późniejszym korzystaniu dodawać +2, by mieć odniesienie do tablicy stanu.
        fillWithValue(availableMoves, 8);
        /*Dodanie dwójek*/
        availableMoves[0][0] = 2;
        availableMoves[0][size-1] = 2;
        availableMoves[size-1][0] = 2;
        availableMoves[size-1][size-1] = 2;
        /*Dodanie czwórek "narożnikowych"*/
        availableMoves[1][1] = 4;
        availableMoves[1][size-2] = 4;
        availableMoves[size-2][1] = 4;
        availableMoves[size-2][size-2] = 4;
        /*Dodanie trójek*/
        availableMoves[1][0] = 3;
        availableMoves[0][1] = 3;
        availableMoves[0][size-2] = 3;
        availableMoves[1][size-1] = 3;
        availableMoves[size-2][0] = 3;
        availableMoves[size-1][1] = 3;
        availableMoves[size-1][size-2] = 3;
        availableMoves[size-2][size-1] = 3;
        /*Dodanie czwórek oraz szóstek*/
        for (int i = 2; i < size-2; i++) {
            availableMoves[i][1] = 6; availableMoves[i][size-2] = 6; availableMoves[1][i] = 6; availableMoves[size-2][i] = 6;
            availableMoves[i][0] = 4; availableMoves[i][size-1] = 4; availableMoves[0][i] = 4; availableMoves[size-1][i] = 4;
        }

        return availableMoves;
    }

    /**
     * Przygotowuje stan początkowy odwiedzoych pól wraz z marginesem.
     * @return Zwrócony stan początkowy.
     */
    private int[][] prepareInitialState(){
        var state = new int[size][size];
        fillWithValue(state, -1);//wypełnienie tablicy odwiedzonych -1 - ustalenie pasa granicznego
        for (int row = 2; row < size - 2; row++) {
            for (int column = 2; column < size - 2; column++) {
                state[row][column] = 0;//wypełnienie 0 pozycji, które mogą zostać odwiedzone przez skoczka.
            }
        }

        return state;
    }
    /**
     * Wypełnia tablicę dwuwymiarową podaną wartością.
     * @param table Tablica dwuwymiarowa do uzupełnienia.
     * @param value Wartość, która ma być wprowadzona do tablicy.
     */
    private void fillWithValue(int[][] table, int value){
        for (int row = 0; row < table.length; row++) {
            for (int column = 0; column < table.length; column++) {
                table[row][column] = value;
            }
        }
    }

    /**
     * Losuje dla skoczka szachowego losową pozycję na szachownicy.
     * <b>Używać tylko przy rozpoczęciu poszukiwania rozwiązania!</b>
     * @return Zwraca nową losową pozycja skoczka na szachownicy.
     */
    private Position setRandomKnightPosition() {
        Random random = new Random();
        var row = random.nextInt(size -4) + 2;//+2 spowodowane przesunięciem
        var column = random.nextInt(size - 4) + 2;//+2 spowodowane przesunięciem
        return new Position(row, column);
    }

    /**
     * Rozwiązuje problem skoczka szachowego.
     * Wyświetla rozwiązanie na ekranie konsoli.
     */
    public void prepareSolution(){
        if(solved && state != null){
            showResult();
            return;
        }

        do{
            Position initialKnightPosition = setRandomKnightPosition();
            state = prepareInitialState();
            availableMoves = prepareAvailableMoves();
            setKnightOnStartPosition(initialKnightPosition, 1);
            System.out.println("Wylosowano nową pozycję początkową skoczka: " + initialKnightPosition);

            tryNextMove(initialKnightPosition, 2);
        }while (!solved);

        showResult();
    }

    /**
     * Główna metoda rozwiązująca problem skoczka.
     * @param knightPosition Przekazana, obecna dla iteracji pozycja skoczka.
     * @param moveNumber Numer ruchu do zrealizowania.
     */
    private void tryNextMove(Position knightPosition, int moveNumber){
        var possibleMoves = getPossiblesMoves(knightPosition);

        while (!solved && possibleMoves.size() > 0){
            var move = possibleMoves.poll();
            setKnightOnPosition(move, moveNumber);
            if(moveNumber < fields){
                tryNextMove(move, moveNumber + 1);
                if (!solved){
                    undoKnightMove(move);
                }
            }else solved = true;
        }
    }

    /**
     * Ustawia skoczka na wskazanej pozycji na szachownicy.
     * @param position Pozycja na której ma znaleźć się skoczek.
     */
    private void setKnightOnStartPosition(Position position, int moveNumber){
        state[position.getRow()][position.getColumn()] = moveNumber;
    }

    /**
     * Ustawia skoczka na wskazanej pozycji na szachownicy.
     * @param position Pozycja na której ma znaleźć się skoczek.
     */
    private void setKnightOnPosition(Position position, int moveNumber){
        state[position.getRow()][position.getColumn()] = moveNumber;
        var neighbours = getPossiblesMoves(position);
        //zmniejszamy ilość sąsiadów dla sąsiadów tego pola
        for (var neighbour:neighbours) {
            int row = neighbour.row;
            int column = neighbour.column;

            availableMoves[row - 2][column -2] -= 1;
        }
    }

    /**
     * Wymazuje ruch ze wskazanej pozycji
     * @param position Pozycja, z której ruch ma być wymazany.
     */
    private void undoKnightMove(Position position){
        state[position.getRow()][position.getColumn()] = 0;

        var neighbours = getPossiblesMoves(position);
        //zwiększamy ilość sąsiadów dla sąsiadów tego pola
        for (var neighbour:neighbours) {
            int row = neighbour.row;
            int column = neighbour.column;

            availableMoves[row - 2][column -2] += 1;
        }
    }

    /**
     *
     * @return Zwraca dostępne pozycje w ramach najbliższego ruchu.
     */
    private PriorityQueue<Position> getPossiblesMoves(Position knightPosition){
        PriorityQueue<Position> possiblesMoves = new PriorityQueue<>();

        for (int i = 0; i < 8; i++) {
            int column = knightPosition.getColumn() + columnMove[i];
            int row = knightPosition.getRow() + rowMove[i];

            if(state[row][column] == 0){//pole nie odwiedzone i w granicach
                var moves = availableMoves[row -2][column -2];
                possiblesMoves.add(new Position(row, column, moves));
            }
        }

        return possiblesMoves;
    }

    /**
     * W przystępny sposób wyświetla rozwiązanie.
     */
    public void showResult(){
        if (solved && state != null) {
            var result = this.toString();
            System.out.println(result);
        }else{
            System.out.println("Problem skoczka szachowego nie jest (jeszcze) rozwiązany!");
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if(!solved && state == null){
            return "Problem skoczka szachowego nie jest (jeszcze) rozwiązany!";
        }

        for (int column = 2; column < size -2; column++) {
            for (int row = 2; row < size -2; row++)
                builder.append(String.format("%10d ", state[column][row]));
            builder.append("\n");
        }

        return builder.toString();
    }

    class Position implements Comparable<Position>{
        private int row;
        private int column;
        private int neighbors;

        public Position(int row, int column, int neighbors) {
            this.row = row;
            this.column = column;
            this.neighbors = neighbors;
        }

        public Position(int row, int column) {
            this.row = row;
            this.column = column;
        }

        public int getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public int getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        @Override
        public String toString() {
            return "Wiersz: " + (row -2) + ", Kolumna: " + (column -2);
        }

        @Override
        public int compareTo(Position o) {
            if (this.neighbors == o.neighbors)
                return 0;
            else if (this.neighbors > o.neighbors)
                return 1;
            else
                return -1;
        }
    }
}
