package pl.kaliszewski.algorithms.KnightsTour;

import java.util.Scanner;

public class App
{
    public static void main( String[] args )
    {
        System.out.println( "Proszę podać wymiar szachownicy: " );
        Scanner input = new Scanner(System.in);

        try{
            int size = Integer.parseInt(input.nextLine());
            KnightsTour problem = new KnightsTour(size);
            problem.prepareSolution();
        }catch (Exception e){
            System.out.println("Przechwycono wyjątek: \n" + e.getMessage());
        }
    }
}
