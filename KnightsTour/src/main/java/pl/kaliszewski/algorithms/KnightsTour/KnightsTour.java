package pl.kaliszewski.algorithms.KnightsTour;

import java.util.*;

/**
 * Klasa rozwiązująca Problem skoczka szachowego.
 */
public class KnightsTour {
    /**
     * Stan odwiedzonych pól na szachownicy przez skoczka.
     * Rozwiązanie.
     */
    private int[][] state;
    /**
     * Określa liczbę kolumn oraz wierszy na szachownicy.
     */
    private int size;
    /**
     * Określa czy rozwiązano problem.
     */
    private boolean solved;
    /**
     * Określa liczbę pól na szachownicy.
     */
    private int fields;
    /**
     * Stanowi możliwe zmiany kolumny w następnym ruchu skoczka.
     */
    final int columnMove[] = {2, 1, -1, -2, -2, -1, 1, 2};
    /**
     * Stanowi możliwe zmiany kolumny w następnym ruchu skoczka.
     */
    final int rowMove[] = {1, 2, 2, 1, -1, -2, -2, -1};

    public KnightsTour(int size){
        this.size = size + 4;//obwódka +2 z dwóch stron
        this.fields = size * size;
        state = null;
    }

    /**
     * Przygotowuje stan początkowy odwiedzoych pól wraz z marginesem.
     * @return Zwrócony stan początkowy.
     */
    private int[][] prepareInitialState(){
        var state = new int[size][size];
        fillWithValue(state, -1);//wypełnienie tablicy odwiedzonych -1 - ustalenie pasa granicznego
        for (int row = 2; row < size - 2; row++) {
            for (int column = 2; column < size - 2; column++) {
                state[row][column] = 0;//wypełnienie 0 pozycji, które mogą zostać odwiedzone przez skoczka.
            }
        }

        return state;
    }

    /**
     * Wypełnia tablicę dwuwymiarową podaną wartością.
     * @param table Tablica dwuwymiarowa do uzupełnienia.
     * @param value Wartość, która ma być wprowadzona do tablicy.
     */
    private void fillWithValue(int[][] table, int value){
        for (int row = 0; row < table.length; row++) {
            for (int column = 0; column < table.length; column++) {
                table[row][column] = value;
            }
        }
    }

    /**
     * Losuje dla skoczka szachowego losową pozycję na szachownicy.
     * <b>Używać tylko przy rozpoczęciu poszukiwania rozwiązania!</b>
     * @return Zwraca nową losową pozycja skoczka na szachownicy.
     */
    private Position setRandomKnightPosition() {
        Random random = new Random();
        var row = random.nextInt(size -4) + 2;//+2 spowodowane przesunięciem
        var column = random.nextInt(size - 4) + 2;//+2 spowodowane przesunięciem
        return new Position(row, column);
    }

    /**
     * Rozwiązuje problem skoczka szachowego.
     * Wyświetla rozwiązanie na ekranie konsoli.
     */
    public void prepareSolution(){
        if(solved && state != null){
            showResult();
            return;
        }

        do{
            Position initialKnightPosition = setRandomKnightPosition();
            state = prepareInitialState();
            setKnightOnPosition(initialKnightPosition, 1);
            System.out.println("Wylosowano nową pozycję początkową skoczka: " + initialKnightPosition);

            tryNextMove(initialKnightPosition, 2);
        }while (!solved);

        showResult();
    }

    /**
     * Główna metoda rozwiązująca problem skoczka.
     * @param knightPosition Przekazana, obecna dla iteracji pozycja skoczka.
     * @param moveNumber Numer ruchu do zrealizowania.
     */
    private void tryNextMove(Position knightPosition, int moveNumber){
        var possibleMoves = getPossiblesMoves(knightPosition, state);
        Iterator<Position> positionIterator = possibleMoves.iterator();

        while (!solved && positionIterator.hasNext()){
            var move = positionIterator.next();
            setKnightOnPosition(move, moveNumber);
            if(moveNumber < fields){
                tryNextMove(move, moveNumber + 1);
                if (!solved){
                    undoKnightMove(move);
                }
            }else solved = true;
        }
    }

    /**
     * Ustawia skoczka na wskazanej pozycji na szachownicy.
     * @param position Pozycja na której ma znaleźć się skoczek.
     */
    private void setKnightOnPosition(Position position, int moveNumber){
        state[position.getRow()][position.getColumn()] = moveNumber;
    }

    /**
     * Wymazuje ruch ze wskazanej pozycji
     * @param position Pozycja, z której ruch ma być wymazany.
     */
    private void undoKnightMove(Position position){
        state[position.getRow()][position.getColumn()] = 0;
    }

    /**
     *
     * @return Zwraca dostępne pozycje w ramach najbliższego ruchu.
     */
    private List<Position> getPossiblesMoves(Position knightPosition, int[][] state){
        List<Position> possiblesMoves = new ArrayList<>(8);

        for (int i = 0; i < 8; i++) {
            int column = knightPosition.getColumn() + columnMove[i];
            int row = knightPosition.getRow() + rowMove[i];

            if(state[row][column] == 0)//pole nie odwiedzone i w granicach
                possiblesMoves.add(new Position(row, column));
        }

        return possiblesMoves;
    }

    /**
     * W przystępny sposób wyświetla rozwiązanie.
     */
    public void showResult(){
        if (solved && state != null) {
            var result = this.toString();
            System.out.println(result);
        }else{
            System.out.println("Problem skoczka szachowego nie jest (jeszcze) rozwiązany!");
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if(!solved && state == null){
            return "Problem skoczka szachowego nie jest (jeszcze) rozwiązany!";
        }

        for (int column = 2; column < size -2; column++) {
            for (int row = 2; row < size -2; row++)
                builder.append(String.format("%10d ", state[column][row]));
            builder.append("\n");
        }

        return builder.toString();
    }

    class Position {
        private int row;
        private int column;

        public Position(int row, int column) {
            this.row = row;
            this.column = column;
        }

        public int getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public int getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        @Override
        public String toString() {
            return "Wiersz: " + (row -2) + ", Kolumna: " + (column -2);
        }
    }
}
