package pl.kaliszewski.algorithms.KnightsTour;

import java.util.Scanner;

public class WarnsdorfsKnightsTourApp
{
    public static void main( String[] args )
    {
        System.out.println( "Problem skoczka szachowego algorytmem Warnsdorfa.\nProszę podać wymiar szachownicy: " );
        Scanner input = new Scanner(System.in);

        try{
            int size = Integer.parseInt(input.nextLine());
            WarnsdorfsKnightsTour problem = new WarnsdorfsKnightsTour(size);
            problem.prepareSolution();
        }catch (Exception e){
            System.out.println("Przechwycono wyjątek: \n" + e.getMessage());
        }
    }
}