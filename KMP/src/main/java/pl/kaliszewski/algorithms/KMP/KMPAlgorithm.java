package pl.kaliszewski.algorithms.KMP;

import java.util.LinkedList;
import java.util.List;

/**
 * Algorytm Knutha-Morrisa-Pratta
 */
public class KMPAlgorithm {

    public static List<Integer> search(String pattern, String text) {
        List<Integer> results = new LinkedList<>();
        char[] patternChar = pattern.toCharArray();
        char[] textChar = text.toCharArray();
        int patternSize = patternChar.length;
        int textSize = textChar.length;
        int[] prefix = initPrefix(patternChar, patternSize);
        int i = 0, j = 0;

        while (i < textSize) {
            if (textChar[i] == patternChar[j]) {
                i++;
                j++;
            }

            if (j == patternSize) {
                results.add(i - j);
                j = prefix[j - 1];
            } else if (i < textSize && patternChar[j] != textChar[i]) {
                if (j != 0)
                    j = prefix[j - 1];
                else
                    i++;
            }
        }

        return results;
    }

    private static int[] initPrefix(char[] pattern, int patternSize) {
        int[] prefix = new int[patternSize];
        int length = 0;
        prefix[0] = 0;

        for (int i = 1; i < patternSize; i++) {
            if (pattern[i] == pattern[length]) {
                length++;
                prefix[i] = length;
            } else {
                if (length != 0) {
                    length = prefix[length - 1];
                    i--;
                } else
                    prefix[i] = 0;
            }
        }

        return prefix;
    }
}
