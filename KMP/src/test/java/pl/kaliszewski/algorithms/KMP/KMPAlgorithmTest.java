package pl.kaliszewski.algorithms.KMP;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class KMPAlgorithmTest {

    @Test
    public void search0() {
        String text = "ABC ABCDAB ABCDABCDABDE";
        String pattern = "ABCDABD";

        try {
            var startPositions = KMPAlgorithm.search(pattern, text);
            Assertions.assertEquals(15, startPositions.get(0));
        }catch (Exception e){
            Assertions.fail(e.getMessage());
        }
    }

    @Test
    public void search1() {
        String text = "AAAABAAAAABBBAAAAB";
        String pattern = "AAAB";
        try {
            var startPositions = KMPAlgorithm.search(pattern, text);
            Assertions.assertEquals(Arrays.asList(1, 7 , 14), startPositions);
        }catch (Exception e){
            Assertions.fail(e.getMessage());
        }
    }
}
