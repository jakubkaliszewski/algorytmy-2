package Graph.DFS;

import Graph.Graph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class BridgesAndArticulationsFindingTest {

    @Test
    void findBridges(){
        List<Bridge> goodResult = Arrays.asList(new Bridge(2,1), new Bridge(5,1), new Bridge(7, 0));
        Graph graph = new Graph(11);
        try {
            for (int i = 0; i < 11; i++) {
                graph.addNode();
            }

            graph.addNeighborForNode(0,1);
            graph.addNeighborForNode(0,7);
            graph.addNeighborForNode(0,6);

            graph.addNeighborForNode(1,5);
            graph.addNeighborForNode(1,6);
            graph.addNeighborForNode(1,2);

            graph.addNeighborForNode(2,3);
            graph.addNeighborForNode(2,4);

            graph.addNeighborForNode(3,4);

            graph.addNeighborForNode(7,8);
            graph.addNeighborForNode(7,10);

            graph.addNeighborForNode(8,9);
            graph.addNeighborForNode(9,10);

            var result = BridgesAndArticulationsFinding.findBridge(graph);
            System.out.println(result);
            if (!result.containsAll(goodResult))
                Assertions.fail();
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    void findArticulationsPoints1(){
        List<Integer> goodResult = Arrays.asList(0, 3);
        Graph graph = new Graph(6);
        try {
            for (int i = 0; i < 6; i++) {
                graph.addNode();
            }

            graph.addNeighborForNode(0,1);
            graph.addNeighborForNode(0,2);
            graph.addNeighborForNode(0,3);

            graph.addNeighborForNode(1,2);

            graph.addNeighborForNode(3,4);
            graph.addNeighborForNode(3,5);
            graph.addNeighborForNode(4,5);

            var result = BridgesAndArticulationsFinding.findArticulationsPoint(graph);
            System.out.println(result);
            if (!result.containsAll(goodResult))
                Assertions.fail();
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    void findArticulationsPoints2(){
        List<Integer> goodResult = Collections.singletonList(1);
        Graph graph = new Graph(7);
        try {
            for (int i = 0; i < 7; i++) {
                graph.addNode();
            }

            graph.addNeighborForNode(0,1);
            graph.addNeighborForNode(0,2);

            graph.addNeighborForNode(1,2);
            graph.addNeighborForNode(1,3);
            graph.addNeighborForNode(1,4);
            graph.addNeighborForNode(1,6);

            graph.addNeighborForNode(3,5);
            graph.addNeighborForNode(3,5);
            graph.addNeighborForNode(4,5);

            var result = BridgesAndArticulationsFinding.findArticulationsPoint(graph);
            System.out.println(result);
            if (!result.containsAll(goodResult))
                Assertions.fail();
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

}
