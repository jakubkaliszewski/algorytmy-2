package Graph;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

class StronglyConnectedComponentTest {

    @Test
    void getStronglyConnectedComponents(){
        Graph graph = new Graph(7);

        try{
            for (int i = 0; i < 7; i++) {
                graph.addNode();
            }
            graph.addNeighborForNode(0, 1, 1, false);
            graph.addNeighborForNode(1, 2, 1, false);
            graph.addNeighborForNode(2, 3, 1, false);
            graph.addNeighborForNode(3, 6, 1, false);
            graph.addNeighborForNode(3, 4, 1, false);
            graph.addNeighborForNode(3, 1, 1, false);
            graph.addNeighborForNode(4, 5);
            graph.addNeighborForNode(6, 1, 1, false);

            var result = StronglyConnectedComponent.getStronglyConnectedComponents(graph);

            if(!result.get(0).contains(0))
                Assertions.fail("Silnie spójna składowa 1 zła!");
            if(!result.get(1).containsAll(Arrays.asList(1,2,3,6)))
                Assertions.fail("Silnie spójna składowa 2 zła!");
            if(!result.get(2).containsAll(Arrays.asList(4,5)))
                Assertions.fail("Silnie spójna składowa 3 zła!");
        }catch (Exception e){
            Assertions.fail(e.getMessage());
        }
    }
}
