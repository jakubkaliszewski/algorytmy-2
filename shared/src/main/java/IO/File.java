package IO;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Zawiera narzędzia do operacji na plikach.
 */
public class File {
    /**
     * Wczytuje spreparowany plik z danymi.
     * @param filename Plik z danymi do wczytania.
     */
    public static List<String> loadDataFromFile(String filename) throws Exception {
        if (filename == null)
            throw new Exception("Nie podano pliku źródłowego!");

        List<String> data;
        try {
            data = Files.readAllLines(Paths.get(filename).toAbsolutePath(), Charset.defaultCharset());
        } catch (IOException exception) {
            System.out.printf("Błąd odczytu pliku %s! Wyjątek: %s\n", filename, exception.toString());
            throw exception;
        }

        return data;
    }
}

