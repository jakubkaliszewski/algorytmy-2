package Tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Tree {
    int size;
    Node[] nodes;

    public Tree(int size) {
        nodes = new Node[size];
        nodes[0] = new Node(0, true);
        for (int i = 1; i < size; i++) {
            nodes[i] = new Node(i, false);
        }
        this.size = size;
    }

    public void addChildren(int parentId, List<Integer> childrenIds) {
        var parent = nodes[parentId];
        List<Node> children = new ArrayList<>(childrenIds.size());
        childrenIds.forEach(id -> {
            var child = nodes[id];
            children.add(child);
        });

        parent.addChildren(children);
    }

    public void addChild(int parentId, int childId) {
        var parent = nodes[parentId];
        var child = nodes[childId];

        parent.addChild(child);
    }

    public int getSize() {
        return size;
    }

    public Node[] getNodes() {
        return nodes;
    }

    public Node getRoot() throws Exception {
        var stream = Arrays.stream(nodes).filter(node -> node.isRoot()).findFirst();
        if(stream.isPresent())
            return stream.get();

        throw new Exception("Drzewo nie posiada zdefiniowanego korzenia!");
    }

    public Node getNode(int nodeId) throws Exception {
        var stream = Arrays.stream(nodes).filter(node -> node.getId() == nodeId).findFirst();
        if(stream.isPresent())
            return stream.get();

        throw new Exception("Drzewo nie posiada węzła o podanym id!");
    }
}
