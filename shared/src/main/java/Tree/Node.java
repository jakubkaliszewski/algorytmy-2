package Tree;

import java.util.LinkedList;
import java.util.List;

public class Node {
    private int id;
    private boolean root;

    private List<Node> children;

    public Node(int id, boolean root) {
        this.id = id;
        children = new LinkedList<Node>();
        this.root = root;
    }

    public int getId() {
        return id;
    }

    public List<Node> getChildren() {
        return children;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public void addChildren(List<Node> children){
        this.children.addAll(children);
    }

    public void addChild(Node child){
        children.add(child);
    }

    public boolean isRoot() {
        return root;
    }

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                ", root=" + root +
                ", children=" + children +
                '}';
    }
}
