package Graph;

import Graph.DFS.DFS;
import Graph.DFS.DFSNodeResult;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementacja silnie spójnych składowych
 */
public class StronglyConnectedComponent {
    public static Map<Integer, List<Integer>> getStronglyConnectedComponents(Graph graph){
        Map<Integer, List<Integer>> components = new HashMap<>();
        var transposeGraph = Graph.getTransposeGraph(graph);

        var graphDFS = DFS.DFS(graph);
        graphDFS.sort(Comparator.comparingInt(DFSNodeResult::getLast).reversed());
        var nodeIds = graphDFS.stream().map(DFSNodeResult::getNodeId).collect(Collectors.toList());

        int componentId = 0;
        List<Integer> visitedNodes = new ArrayList<>(graph.nodesCount);
        for (var node : nodeIds) {
            if(!visitedNodes.contains(node)){
                var result = DFS.DFSFromNode(transposeGraph, node, visitedNodes);
                var nodesInComponent = result.stream().map(DFSNodeResult::getNodeId).collect(Collectors.toList());
                components.put(componentId, nodesInComponent);
                visitedNodes.addAll(nodesInComponent);
                componentId++;
            }
        }

        return components;
    }
}
