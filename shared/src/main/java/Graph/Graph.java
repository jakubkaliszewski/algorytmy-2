package Graph;

import java.util.*;

public class Graph {
    protected int nodesCount;
    protected int size;
    int limit;
    protected int[][] matrix;

    public Graph(int initialSize) {
        this.size = initialSize;
        this.nodesCount = 0;
        this.limit = 0;
        this.matrix = new int[initialSize][initialSize];
    }

    public Graph(Graph graph){
        this.size = graph.size;
        this.nodesCount = graph.nodesCount;
        this.limit = graph.limit;
        this.matrix = graph.matrix.clone();
    }

    public int addNode() throws Exception {
        int index = -1;
        if (nodesCount+1 > size){
            System.out.println("Brak miejsca na dodanie kolejnego wierzchołka. Nastąpi przepisanie do większej struktury!");
            resize(this.size * 2);
        }
        //szukamy miejsca po usuniętym elemencie
        for (int i = 0; i < nodesCount; i++) {
            if (matrix[i][i] == 0){
                matrix[i][i] = 1;
                index = i;
                break;
            }
        }
        if(index == -1){
            if (matrix[nodesCount][nodesCount] == 0){
                matrix[nodesCount][nodesCount] = 1;
                index = nodesCount;
                limit = nodesCount;
            }
        }

        if(index == -1){
            throw new Exception("Nie dodano nowego wierzchołka!");
        }

        this.nodesCount++;
        System.out.printf("Dodany wierzchołek ma indeks: %d\n", index);
        return index;
    }

    public void removeNode(int nodeId){
        this.matrix[nodeId][nodeId] = 0;

        //usunięcie także połączeń z sąsiadami
        var neighbors = getNeighbors(nodeId);
        for (var neighbor : neighbors) {
            this.matrix[nodeId][neighbor] = 0;
            this.matrix[neighbor][nodeId] = 0;
        }
    }

    public void addNeighborForNode(int nodeId, int newNeighbor) throws Exception {
        addNeighborForNode(nodeId, newNeighbor, 1, true);
    }

    public void addNeighborForNode(int nodeId, int newNeighbor, int cost) throws Exception {
        addNeighborForNode(nodeId, newNeighbor, cost, true);
    }

    //wiadomość o sąsiadach są w wierszach
    public void addNeighborForNode(int nodeId, int newNeighbor, int cost, boolean isBidirect) throws Exception {
        if (!isNodeExist(nodeId) || !isNodeExist(newNeighbor)){
            throw new Exception(String.format("Nie dodano połączenia między %d a %d. Jeden z elementów nie istnieje!", nodeId, newNeighbor));
        }

        this.matrix[nodeId][newNeighbor] = cost;
        if (isBidirect){
            this.matrix[newNeighbor][nodeId] = cost;
        }
    }

    public boolean isNodeExist(int nodeId){
        return nodeId < size && this.matrix[nodeId][nodeId] == 1;
    }

    public List<Integer> getNeighbors(int nodeId){
        List<Integer> neighbors = new ArrayList<>(nodesCount);
        int nodes = 0;
        for (int column = 0; column < this.size; column++) {
            if(column == nodeId)
                continue;
            if(matrix[nodeId][column] != 0){
                neighbors.add(column);
                nodes++;
                if(nodes == nodesCount)
                    break;
            }
        }

        return neighbors;
    }

    public int getNodesCount() {
        return nodesCount;
    }

    public int getSize() {
        return size;
    }


    public List<Edge> getEdges(){
        List<Edge> edges = new ArrayList<>(nodesCount);
        for (int row = 0; row < size; row++) {
            for (int column = 0; column < size; column++) {
                if(column != row && matrix[row][column] > 0){
                    var edge = new Edge(row, column, matrix[row][column]);
                    edges.add(edge);
                }
            }
        }

        return edges;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (int row = 0; row < nodesCount; row++) {
            for (int column = 0; column < nodesCount; column++) {
                builder.append(" | ").append(this.matrix[column][row]);
            }
            builder.append(" |\n");
        }

        return builder.toString();
    }

    protected void resize(int newSize){
        var actualMatrix = this.matrix.clone();
        this.matrix = new int[newSize][newSize];
        this.size = newSize;

        for (int column = 0; column < actualMatrix.length; column++) {
            for (int row = 0; row < actualMatrix.length; row++) {
                this.matrix[column][row] = actualMatrix[column][row];
            }
        }
    }

    /**
     * Zwraca listę identyfikatorów istniejących wierzchołków w grafie.
     * @return Lista identyfikatorów wierzchołków w grafie.
     */
    public List<Integer> getNodesId() {
        List<Integer> nodesId = new ArrayList<>(nodesCount);
        int visitedNodes = 0;
        for (int i = 0; i < size; i++) {
            if (matrix[i][i] == 1){
                nodesId.add(i);
                visitedNodes++;
            }else if(visitedNodes == nodesCount){
                break;
            }
        }

        return nodesId;
    }

    /**
     * Wykonuje transpozycję grafu.
     * @param graph Graf, który ma być transponowany.
     * @return Graf po transpozycji.
     */
    public static Graph getTransposeGraph(Graph graph){
        Graph transposedGraph = new Graph(graph.size);
        var matrix = graph.matrix;
        int size = matrix.length;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                transposedGraph.matrix[i][j] = matrix[j][i];
            }
        }

        return transposedGraph;
    }
}
