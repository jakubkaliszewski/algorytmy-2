package Graph.DFS;

import Graph.Graph;

import java.util.ArrayList;
import java.util.List;

public class DFS {
    public static List<DFSNodeResult> DFS(Graph graph) {
        int possibleNodesCount = graph.getSize();
        DFSStructure operationStructure = new DFSStructure(possibleNodesCount);
        graph.getNodesId().forEach(element -> {
            if(operationStructure.color[element] == ColorEnum.WHITE.getValue())
                visitNode(element, graph, operationStructure);
        });

        return buildResults(operationStructure, 0);
    }

    public static List<DFSNodeResult> DFSFromNode(Graph graph, int nodeId, List<Integer> vistitedNodes) {
        int possibleNodesCount = graph.getSize();
        DFSStructure operationStructure = new DFSStructure(possibleNodesCount);
        for (var node : vistitedNodes) {
            operationStructure.color[node] = ColorEnum.BLACK.getValue();
        }
        visitNode(nodeId, graph, operationStructure);
        return buildResults(operationStructure, nodeId);
    }

    private static void visitNode(int nodeId, Graph graph, DFSStructure structure) {//czasy są złe?
        structure.color[nodeId] = ColorEnum.GRAY.getValue();
        structure.time++;
        structure.first[nodeId] = structure.time;//wejście do węzła

        var neighbors = graph.getNeighbors(nodeId);
        for (var neighbor : neighbors) {
            if(structure.color[neighbor] == ColorEnum.WHITE.getValue()){
                structure.parent[neighbor] = nodeId;
                visitNode(neighbor, graph, structure);
            }
        }

        structure.color[nodeId] = ColorEnum.BLACK.getValue();
        structure.time++;
        structure.last[nodeId] = structure.time;//wyjście z węzła
    }

    private static List<DFSNodeResult> buildResults(DFSStructure operationStructure, int initialNode) {
        List<DFSNodeResult> resultsList = new ArrayList<>(operationStructure.nodesCount);
        for (int index = initialNode; index < operationStructure.nodesCount; index++) {
            if(operationStructure.first[index] != 0 && operationStructure.last[index] != 0){
                var result = new DFSNodeResult(
                        operationStructure.first[index],
                        operationStructure.last[index],
                        operationStructure.parent[index],
                        index
                );
                resultsList.add(result);
            }
        }

        return resultsList;
    }
}
