package Graph.DFS;

public class Bridge {
    private int nodeId;
    private int parentId;

    public Bridge(int nodeId, int parentId) {
        this.nodeId = nodeId;
        this.parentId = parentId;
    }

    public int getNodeId() {
        return nodeId;
    }

    public int getParentId() {
        return parentId;
    }

    @Override
    public String toString() {
        return "Bridge{" +
                "nodeId=" + nodeId +
                ", parentId=" + parentId +
                '}';
    }


    @Override
    public boolean equals(Object obj) {
        var b1 = (Bridge)obj;
        if(b1.parentId == this.parentId && b1.nodeId == this.nodeId)
            return true;
        return false;
    }
}
