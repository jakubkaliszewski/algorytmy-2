package Graph.DFS;

public class DFSNodeResult{
    /**
     * Moment wejścia w węzeł w trakcie DFS
     */
    final int first;
    /**
     * Moment wyjścia z węzła w trakcie DFS
     */
    final int last;
    final int parentId;
    /**
     * Id węzła
     */
    final int nodeId;

    public DFSNodeResult(int first, int last, int parent, int nodeId) {
        this.first = first;
        this.last = last;
        this.parentId = parent;
        this.nodeId = nodeId;
    }

    public int getFirst() {
        return first;
    }

    public int getLast() {
        return last;
    }

    public int getParentId() {
        return parentId;
    }

    public int getNodeId() {
        return nodeId;
    }
}
