package Graph.DFS;

public enum ColorEnum {
    WHITE(0),
    GRAY(1),
    BLACK(2);

    private final int value;

    ColorEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    ColorEnum getColorEnumFromValue(int value) throws Exception {
        for (var v : ColorEnum.values()) {
            if(value == v.value)
                return v;
        }

        throw new Exception("Nie znaleziono wartości enumeratora!");
    }
}
