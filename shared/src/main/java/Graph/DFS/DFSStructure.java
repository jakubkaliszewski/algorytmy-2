package Graph.DFS;

public class DFSStructure {
    int[] color;
    int[] first;
    int[] last;
    int[] parent;
    int time;
    int nodesCount;

    public DFSStructure(int nodesCount) {
        color = new int[nodesCount];
        first = new int[nodesCount];
        last = new int[nodesCount];
        parent = new int[nodesCount];
        time = 0;
        this.nodesCount = nodesCount;
    }
}
