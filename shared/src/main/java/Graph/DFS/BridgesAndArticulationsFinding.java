package Graph.DFS;

import Graph.Graph;

import java.util.ArrayList;
import java.util.List;

public class BridgesAndArticulationsFinding {

    public static List<Integer> findArticulationsPoint(Graph graph){
        int possibleNodesCount = graph.getSize();
        BridgeStructure operationStructure = new BridgeStructure(possibleNodesCount);
        graph.getNodesId().forEach(element -> {
            if (operationStructure.color[element] == ColorEnum.WHITE.getValue())
                visitNode(element, graph, operationStructure);
        });

        return buildArticulationsPointResults(operationStructure);
    }

    public static List<Bridge> findBridge(Graph graph) {
        int possibleNodesCount = graph.getSize();
        BridgeStructure operationStructure = new BridgeStructure(possibleNodesCount);
        graph.getNodesId().forEach(element -> {
            if (operationStructure.color[element] == ColorEnum.WHITE.getValue())
                visitNode(element, graph, operationStructure);
        });

        return buildBridgeResults(operationStructure);
    }

    private static void visitNode(int nodeId, Graph graph, BridgeStructure structure) {
        structure.color[nodeId] = ColorEnum.GRAY.getValue();
        structure.time++;
        structure.first[nodeId] = structure.time;//wejście do węzła
        structure.low[nodeId] = structure.time;

        var neighbors = graph.getNeighbors(nodeId);
        for (var neighbor : neighbors) {
            if(structure.parent[nodeId] != neighbor){
                if (structure.color[neighbor] == ColorEnum.WHITE.getValue()) {
                    structure.parent[neighbor] = nodeId;
                    visitNode(neighbor, graph, structure);
                    structure.low[nodeId] = Math.min(structure.low[nodeId], structure.low[neighbor]);
                } else {
                    structure.low[nodeId] = Math.min(structure.low[nodeId], structure.first[neighbor]);
                }
            }

        }

        structure.color[nodeId] = ColorEnum.BLACK.getValue();
    }

    private static List<Bridge> buildBridgeResults(BridgeStructure operationStructure) {
        List<Bridge> resultsList = new ArrayList<>();
        for (int index = 0; index < operationStructure.nodesCount; index++) {
            if(index != operationStructure.parent[index]){
                if (operationStructure.first[index] != 0 && operationStructure.low[index] == operationStructure.first[index]) {
                    var result = new Bridge(index, operationStructure.parent[index]);
                    resultsList.add(result);
                }
            }
        }

        return resultsList;
    }

    private static List<Integer> buildArticulationsPointResults(BridgeStructure operationStructure) {
        List<Integer> resultsList = new ArrayList<>();
        for (int index = 0; index < operationStructure.nodesCount; index++) {
            if(index == operationStructure.parent[index]) {//jest korzen
                if (getSonsForNode(operationStructure, index).size() >= 2)//wiecej niz 1 syn
                    resultsList.add(index);
            }
            else{//nie korzen
                var sons = getSonsForNode(operationStructure, index);
                for(var son : sons){
                    if(operationStructure.low[son] >= operationStructure.first[index]){//jeden z synów spełnia warunek low(syn) >= first(index)
                        resultsList.add(index);
                        break;
                    }
                }
            }
        }

        return resultsList;
    }

    private static List<Integer> getSonsForNode(BridgeStructure bridgeStructure, int parentId){
        List<Integer> sonsList = new ArrayList<>();
        for (int i = 0; i < bridgeStructure.nodesCount; i++) {
            if(bridgeStructure.parent[i] == parentId && bridgeStructure.parent[i] != i){
                sonsList.add(i);
            }
        }
        return sonsList;
    }
}
