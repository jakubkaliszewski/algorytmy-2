package Graph.DFS;

public class BridgeStructure extends DFSStructure {
    int[] low;

    public BridgeStructure(int nodesCount) {
        super(nodesCount);
        low = new int[nodesCount];
    }
}
