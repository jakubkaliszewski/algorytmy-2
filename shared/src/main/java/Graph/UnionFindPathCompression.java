package Graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implementacja algorytmu Kruskala z union-Find opartym o drzewa z kompresją ścieżki (log*).
 */
public class UnionFindPathCompression {

    protected final int NO_FATHER = -1;
    /**
     * Liczność zbioru, któremu odpowiada i-ty węzeł.
     */
    protected int[] count;
    /**
     * Nazwa zbioru i-tego elementu.
     */
    protected int[] name;
    /**
     * Ojciec i-tego elementu.
     */
    protected int[] father;
    /**
     * Id węzła będącego korzeniem i-tego zbioru.
     */
    protected int[] root;
    /**
     * Liczba wszystkich elementów ogółem.
     */
    protected int size;

    public UnionFindPathCompression(int size) {
        this.size = size;
        init();
    }

    public UnionFindPathCompression(List<Integer> initialSet) {
        size = initialSet.size();
        init();

        for (int i = 0; i < name.length; i++) {
            name[i] = initialSet.get(i);
        }
    }

    private void init(){
        count = new int[size];
        Arrays.fill(count, 1);
        name = new int[size];

        root = new int[size];
        for (int i = 0; i < size; i++) {
            root[i] = i;
        }
        father = new int[size];
        Arrays.fill(father, NO_FATHER);
    }

    public int find(int searched){
        int node = searched;
        List<Integer> vertices = new ArrayList<>(size);
        while (father[node] != NO_FATHER){//problem z wyjściem tutaj
            vertices.add(father[node]);
            node = father[node];
        }

        root[searched] = node;
        father[searched] = node;

        for (var vertex :vertices) {
            father[vertex] = node;
        }

        return name[node];
    }

    public void union(int i, int j){
        if(count[root[i]] > count[root[j]]){
            var tmp = i;
            i = j;
            j = tmp;
        }
        int large = root[j];
        int small = root[i];

        father[small] = large;
        count[large] = count[large] + count[small];
        count[small] = 0;
        name[small] = large;
        root[small] = large;
    }

    @Override
    public String toString() {
        return "UnionFindPathCompression{" +
                "count=" + Arrays.toString(count) +
                ", name=" + Arrays.toString(name) +
                ", father=" + Arrays.toString(father) +
                ", root=" + Arrays.toString(root) +
                ", size=" + size +
                '}';
    }

    public Graph convertToGraph() throws Exception {
        Graph graph = new Graph(size);
        for (int i = 0; i < size; i++) {
            graph.addNode();
        }

        for (int i = 0; i < size; i++) {
            if(father[i] != -1){
                graph.addNeighborForNode(i, father[i]);
            }
        }

        return graph;
    }
}
