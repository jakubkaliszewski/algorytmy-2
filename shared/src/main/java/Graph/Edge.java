package Graph;

public class Edge {
    final int firstNode;
    final int secondNode;
    final int cost;

    public Edge(int firstNode, int secondNode, int cost) {
        this.firstNode = firstNode;
        this.secondNode = secondNode;
        this.cost = cost;
    }

    public int getFirstNode() {
        return firstNode;
    }

    public int getSecondNode() {
        return secondNode;
    }

    public int getCost() {
        return cost;
    }

    public String getFirstNodeName(){
        return String.valueOf(firstNode);
    }

    public String getSecondNodeName(){
        return String.valueOf(secondNode);
    }

    @Override
    public String toString() {
        return String.format("%s%s : %d", getFirstNodeName(), getSecondNodeName(), cost);
    }
}
