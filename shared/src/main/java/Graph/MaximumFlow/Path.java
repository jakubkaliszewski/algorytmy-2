package Graph.MaximumFlow;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

class Path{
    final int[] nodes;
    final int minimalResidualCapacity;

    public Path(int[] parents, int[] CFPath, int destinationNode) {
        //zakładamy, że ostatni jest destinationNode
        List<Integer> nodesList = new LinkedList<>();
        nodesList.add(destinationNode);

        int parent = destinationNode;
        int edgeFlow;
        int minimumFlow = Integer.MAX_VALUE;
        while (parent != Integer.MIN_VALUE && parents[parent] != -1){
            nodesList.add(parents[parent]);
            edgeFlow = CFPath[parent];
            if (edgeFlow < minimumFlow)
                minimumFlow = edgeFlow;
            parent = parents[parent];
        }
        //odwracamy by zaczynała się ścieżka od wierzchołka startowego
        Collections.reverse(nodesList);
        nodes = nodesList.stream().mapToInt(i->i).toArray();
        minimalResidualCapacity = minimumFlow;
    }

    public int[] getNodes() {
        return nodes;
    }

    public int getMinimalResidualCapacity() {
        return minimalResidualCapacity;
    }
}
