package Graph.MaximumFlow;

import Graph.Edge;

public class ResidualEdge extends Edge {
    private final int flow;

    public ResidualEdge(int firstNode, int secondNode, int cost, int flow) {
        super(firstNode, secondNode, cost);
        this.flow = flow;
    }

    public int getFlow() {
        return flow;
    }

    @Override
    public String toString() {
        return String.format("%s%s : %d / %d", getFirstNodeName(), getSecondNodeName(), getFlow(), getCost());
    }
}
