package Graph.MaximumFlow;

import Graph.Edge;
import Graph.Graph;

import java.util.ArrayList;
import java.util.List;

public abstract class MaximumFlowGraph extends Graph {
    int[][] actualFlow;
    int maximalFlow;

    public MaximumFlowGraph(int initialSize) {
        super(initialSize);
        actualFlow = new int[initialSize][initialSize];
        maximalFlow = 0;
    }

    public MaximumFlowGraph(Graph graph) {
        super(graph);
        actualFlow = new int[size][size];
        maximalFlow = 0;
    }

    protected static int getResidualCapacity(MaximumFlowGraph graph, int node1, int node2) {
        return graph.matrix[node1][node2] - graph.actualFlow[node1][node2];
    }

    @Override
    public List<Edge> getEdges() {
        List<Edge> edges = new ArrayList<>(nodesCount);
        for (int row = 0; row < size; row++) {
            for (int column = 0; column < size; column++) {
                if (column != row && matrix[row][column] > 0) {
                    var edge = new ResidualEdge(row, column, matrix[row][column], actualFlow[row][column]);
                    edges.add(edge);
                }
            }
        }

        return edges;
    }
}
