package Graph.MaximumFlow;

import Graph.Graph;

import java.util.*;

public final class PushRebelMaximumFlowGraph extends MaximumFlowGraph {
    /**
     * Różnica między przepływem wpływającym do wierzchołka a opuszczającym go. Aktualna zajętość wierzchołka.
     * Nadmiarowy przepływ.
     */
    int [] excess;
    /**
     * Wysokość dla każdego wierzchołka.
     */
    int [] height;
    /**
     * Wierzchołki odwiedzone od czasu ostatniego wywołania relabel.
     */
    int [] seen;
    /**
     * Lista wierzchołku do odwiedzenia w grafie -> w celu usunięcia nadmiarów z nich.
     */
    LinkedList<Integer> nodes;

    public PushRebelMaximumFlowGraph(int initialSize) {
        super(initialSize);
        excess = new int[initialSize];
        height = new int[initialSize];
        seen = new int[initialSize];
    }

    public PushRebelMaximumFlowGraph(Graph graph) {
        super(graph);
        excess = new int[graph.getSize()];
        height = new int[graph.getSize()];
        seen = new int[graph.getSize()];
    }

    public static int processPushRebelMaximumFlow(PushRebelMaximumFlowGraph graph, int startNodeId, int destinationNodeId) {
        graph.nodes = new LinkedList<>(graph.getNodesId());
        graph.nodes.removeIf(node -> node == startNodeId || node == destinationNodeId);
        graph.height[startNodeId] = graph.nodesCount;
        graph.excess[startNodeId] = Integer.MAX_VALUE;

        /*Wypchnięcie z wierzhołka startowego.*/
        for (var node : graph.getNodesId()) {
            push(graph, startNodeId, node);
        }

        /*Główna pętla zdejmująca nadmiary z wierzchołków przepychając je dalej.*/
        int pointer = 0;
        while (pointer < graph.nodes.size()){
            int node = graph.nodes.get(pointer);
            int actualHeight = graph.height[node];
            discharge(graph, node);
            if (graph.height[node] > actualHeight) {
                graph.nodes.push(graph.nodes.pop());//dodano na 1. pozycję wierzchołek, który podwyższono
                pointer = 0;//pobieramy z listy 0 pozycję
            }else pointer++;//dodawane, gdy wierzchołek o id w kolejece counter nie ma do usunięcia nadmiarów.
        }

        graph.maximalFlow = 0;
        for(var nodeId : graph.getNodesId()){
            graph.maximalFlow += graph.actualFlow[startNodeId][nodeId];
        }

        return graph.maximalFlow;
    }

    /**
     * Przesłanie przepływu przez sieć do wierzchołka z niższą wysokością
     * @param graph Graf z rozwiązaniem.
     * @param node1 Wierzchołek, z którego przesyłamy przepływ.
     * @param node2 Wierzchołek, do którego przesyłamy przepływ.
     */
    private static void push(PushRebelMaximumFlowGraph graph, int node1, int node2){
        int send = Math.min(graph.excess[node1], getResidualCapacity(graph, node1, node2));
        graph.actualFlow[node1][node2] += send;
        graph.actualFlow[node2][node1] -= send;
        graph.excess[node1] -= send;
        graph.excess[node2] += send;
    }

    /**
     * Dostosowanie wysokości wierzchołków dla sąsiadów wierzchołka node1.
     * Trzymanie niezbędnej koncepcji, że dla krawędzi residualnej height(node1) <= height(node2) + 1.
     * @param graph Graf z rozwiązaniem.
     * @param node1 Wierzchołek, dla którego krawędzi residualnych sprawdzamy warunek <i>valid labelling</i>.
     */
    private static void relabel(PushRebelMaximumFlowGraph graph, int node1){
        int minHeight = Integer.MAX_VALUE;
        for(var node2 : graph.getNodesId()){
            if(getResidualCapacity(graph, node1, node2) > 0){//wykonanie akcji dla sąsiada -> krawędź residualna
                minHeight = Math.min(minHeight, graph.height[node2]);
                graph.height[node1] = minHeight + 1;
            }
        }
    }

    /**
     * Rozładowanie nadmiaru z wierzchołka. Wykonane tylko dla wierzchołka z nadmiarem > 0.
     * @param graph Graf z rozwiązaniem.
     * @param node Wierzchołek, z którego rozładowywany jest nadmiar.
     */
    private static void discharge(PushRebelMaximumFlowGraph graph, int node) {
        while (graph.excess[node] > 0) {//sprawdzamy czy wierzchołek posiada nadmiar
            if (graph.seen[node] < graph.nodesCount) {
                int secondNode = graph.seen[node];
                if (getResidualCapacity(graph, node, secondNode) > 0 && graph.height[node] > graph.height[secondNode]) {
                    push(graph, node, secondNode);
                } else graph.seen[node] += 1;
            } else {
                relabel(graph, node);
                graph.seen[node] = 0;
            }
        }
    }
}
