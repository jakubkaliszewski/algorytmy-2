package Graph.MaximumFlow;

import Graph.Graph;
import java.util.*;

public final class EdmondKarpMaximumFlowGraph extends MaximumFlowGraph {

    public EdmondKarpMaximumFlowGraph(int initialSize) {
        super(initialSize);
    }

    public EdmondKarpMaximumFlowGraph(Graph graph) {
        super(graph);
    }

    @Override
    protected void resize(int newSize) {
        super.resize(newSize);
        var actualFlow = this.actualFlow.clone();
        this.actualFlow = new int[newSize][newSize];

        for (int column = 0; column < actualFlow.length; column++) {
            for (int row = 0; row < actualFlow.length; row++) {
                this.actualFlow[column][row] = actualFlow[column][row];
            }
        }
    }

    public static void processEdmondsKarpMaximumFlow(EdmondKarpMaximumFlowGraph graph, int startNodeId, int destinationNodeId) {
        Path path;
        while (true) {
            path = findAugmentingPath(graph, startNodeId, destinationNodeId);
            if (path == null)
                break;

            updateResidualGraphFlow(graph, path);
        }

        System.out.println("Otrzymany maksymalny przepływ w grafie: " + graph.maximalFlow);
    }

    private static Path findAugmentingPath(EdmondKarpMaximumFlowGraph graph, int startNodeId, int destinationNodeId) {
        int[] parents = new int[graph.size];
        Arrays.fill(parents, Integer.MIN_VALUE);// wierzchołki nieodwiedzone mają MIN_VALUE
        int[] CFPath = new int[graph.size];
        Queue<Integer> queue = new LinkedList<>();
        List<Integer> visitedNodes = new ArrayList<>(graph.nodesCount);

        queue.add(startNodeId);
        visitedNodes.add(startNodeId);
        parents[startNodeId] = -1;//dla startowego

        while (!queue.isEmpty()) {
            int node = queue.poll();
            for (int neighbor : graph.getNeighbors(node)) {
                if (!visitedNodes.contains((neighbor))) {
                    var residualCapacity = getResidualCapacity(graph, node, neighbor);
                    if (residualCapacity > 0) {
                        queue.add(neighbor);
                        parents[neighbor] = node;
                        CFPath[neighbor] = residualCapacity;
                        visitedNodes.add(neighbor);
                    }
                }
                if (neighbor == destinationNodeId)
                    break;
            }
        }

        if (parents[destinationNodeId] == Integer.MIN_VALUE) {
            System.out.println("Nie znaleziono ścieżki powiększającej!");
            return null;
        }

        return new Path(parents, CFPath, destinationNodeId);
    }

    private static void updateResidualGraphFlow(EdmondKarpMaximumFlowGraph graph, Path path) {
        var nodes = path.getNodes();
        var residualCapacity = path.getMinimalResidualCapacity();
        for (int i = 0; i < nodes.length - 1; i++) {
            graph.actualFlow[nodes[i]][nodes[i + 1]] += residualCapacity;
            graph.actualFlow[nodes[i + 1]][nodes[i]] += residualCapacity;
        }
        graph.maximalFlow += residualCapacity;
    }
}
