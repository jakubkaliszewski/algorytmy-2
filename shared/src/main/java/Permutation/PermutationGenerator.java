package Permutation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PermutationGenerator{
    public static List<Integer> getPermutationSet(int size, int minimum, int maximum){
        List<Integer> usedNumbers = new ArrayList<>(size);
        Random random = new Random();
        int number;

        for (int i = 0; i < size; i++) {
            do {
                number = random.nextInt(maximum + 1);
            } while (usedNumbers.contains(number));

            usedNumbers.add(number);
        }

        return usedNumbers;
    }
}


