package pl.kaliszewski.algorithms.Tarjan;

import Graph.DFS.ColorEnum;
import Tree.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TarjanAlgorithm {

    private int[] size;
    private int[] parent;
    private int[] ancestor;
    private int[] color;
    private Tree tree;
    private List<Pair> pairs;

    public TarjanAlgorithm(Tree tree) {
        int treeSize = tree.getSize();
        size = new int[treeSize];
        Arrays.fill(size, 1);

        parent = new int[treeSize];
        for (int i = 0; i < treeSize; i++) {
            parent[i] = i;
        }

        ancestor = new int[treeSize];
        color = new int[treeSize];//0 - white color
        this.tree = tree;
    }

    /**
     * Otrzymaj wspólnego przodka dla każdej podanej pary wierzchołków.
     * @param pairs Lista par, dla których chcemy uzyskać wspólnych przodków.
     * @return Słownik para wraz z rezultatem będącym wspólnym przodkiem.
     * @throws Exception Wyjątek rzucany, gdy w drzewo na którym działamy nie posiada zdefiniowanego korzenia.
     */
    public Map<Pair, Node> getAncestorFor(List<Pair> pairs) throws Exception {
        Map<Pair, Node> results = new HashMap<>();
        this.pairs = pairs;
        //zaczynamy od korzenia drzewa
        tarjan(tree.getRoot(), results);

        return results;
    }

    /**
     * Właściwa implementacja alg Tarjana dla wpólnego przodka.
     * @param node Analizowany węzeł - zaczynamy od wywołania na korzeniu drzewa.
     * @param results Słownik zawierający rezultat na każdą z par.
     */
    private void tarjan(Node node, Map<Pair, Node> results) {
        var nodeId = node.getId();
        makeSet(nodeId);
        ancestor[nodeId] = nodeId;

        for (var child : node.getChildren()) {
            tarjan(child, results);
            union(nodeId, child.getId());
            var found = find(nodeId);
            ancestor[found] = nodeId;
        }

        color[nodeId] = ColorEnum.BLACK.getValue();
        pairs.stream().filter(pair -> pair.getFirstNodeId() == nodeId || pair.getSecondNodeId() == nodeId).collect(Collectors.toList()).forEach(pair -> {
            int secondNode;
            if (pair.getFirstNodeId() == nodeId)
                secondNode = pair.getSecondNodeId();
            else
                secondNode = pair.getFirstNodeId();

            if (color[secondNode] == ColorEnum.BLACK.getValue()) {
                try {
                    var resultNode = tree.getNode(ancestor[find(secondNode)]);
                    results.put(pair, resultNode);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        });
    }

    /**
     * Tworzy nowy zbiór o indeksie x, jeśli nie istnieje.
     * @param x indeks zbioru do utworzenienia.
     */
    private void makeSet(int x) {
        if (size[x] == 0) { // awaryjnej sytuacji nastąpi, raczej powinno być wykonane w konstruktorze
            parent[x] = x;
            size[x] = 1;
        }
    }

    /**
     * Łączy zbiory zadanych elementów. Raczej powinny należeć do różnych zbiorów.
     * @param x element pierwszy.
     * @param y element drugi.
     */
    private void union(int x, int y) {
        int xRoot = find(x);
        int yRoot = find(y);

        //elementy istnieją w tym samym zbiorze
        if (xRoot == yRoot)
            return;

        //elementy nie są w tym  samym zbiorze. Należy je połączyć
        if (size[xRoot] < size[yRoot]) {//zamiana
            int tmp = xRoot;
            xRoot = yRoot;
            yRoot = tmp;
        }

        //łączymy yRoot do xRoot
        parent[yRoot] = xRoot;
        size[xRoot] += size[yRoot];
    }

    /**
     * Find z kompresją ścieżki
     *
     * @param x element, dla którego odnajdujemy jego przynależność dozbioru.
     * @return Numer zbioru, do którego przynależy podany element x.
     */
    private int find(int x) {
        int root = x;
        while (parent[root] != root)
            root = parent[root];

        int father;
        while (parent[x] != root) {
            father = parent[x];
            parent[x] = root;
            x = father;
        }

        return root;
    }
}
