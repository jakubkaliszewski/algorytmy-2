package pl.kaliszewski.algorithms.Tarjan;

public class Pair {
    private int firstNodeId;
    private int secondNodeId;

    public Pair(int firstNodeId, int secondNodeId) {
        this.firstNodeId = firstNodeId;
        this.secondNodeId = secondNodeId;
    }

    public int getFirstNodeId() {
        return firstNodeId;
    }

    public int getSecondNodeId() {
        return secondNodeId;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "firstNodeId=" + firstNodeId +
                ", secondNodeId=" + secondNodeId +
                '}';
    }
}
