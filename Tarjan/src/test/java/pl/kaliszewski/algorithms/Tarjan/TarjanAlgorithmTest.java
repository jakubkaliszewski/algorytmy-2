package pl.kaliszewski.algorithms.Tarjan;

import Tree.Tree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class TarjanAlgorithmTest {

    @Test
    void getParentFor3_5(){
        Tree tree = new Tree(15);
        tree.addChildren(0, Arrays.asList(1,2));
        tree.addChildren(1, Arrays.asList(3,5));
        tree.addChildren(2, Arrays.asList(9,10));
        tree.addChild(3, 4);
        tree.addChildren(5, Arrays.asList(6,7));
        tree.addChild(9, 13);
        tree.addChildren(10, Arrays.asList(11,12));
        tree.addChild(6, 8);
        tree.addChild(11, 14);

        Pair pair = new Pair(5,3);
        List<Pair> pairs = Arrays.asList(pair);

        try {
            TarjanAlgorithm tarjanAlgorithm = new TarjanAlgorithm(tree);
            var result = tarjanAlgorithm.getAncestorFor(pairs);
            Assertions.assertEquals(1, result.get(pair).getId());
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail(e.getMessage());
        }
    }

    @Test
    void getParentFor14_13(){
        Tree tree = new Tree(15);
        tree.addChildren(0, Arrays.asList(1,2));
        tree.addChildren(1, Arrays.asList(3,5));
        tree.addChildren(2, Arrays.asList(9,10));
        tree.addChild(3, 4);
        tree.addChildren(5, Arrays.asList(6,7));
        tree.addChild(9, 13);
        tree.addChildren(10, Arrays.asList(11,12));
        tree.addChild(6, 8);
        tree.addChild(11, 14);

        Pair pair = new Pair(14,13);
        List<Pair> pairs = Arrays.asList(pair);

        try {
            TarjanAlgorithm tarjanAlgorithm = new TarjanAlgorithm(tree);
            var result = tarjanAlgorithm.getAncestorFor(pairs);
            Assertions.assertEquals(2, result.get(pair).getId());
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail(e.getMessage());
        }
    }

    @Test
    void getParentFor12_3(){
        Tree tree = new Tree(15);
        tree.addChildren(0, Arrays.asList(1,2));
        tree.addChildren(1, Arrays.asList(3,5));
        tree.addChildren(2, Arrays.asList(9,10));
        tree.addChild(3, 4);
        tree.addChildren(5, Arrays.asList(6,7));
        tree.addChild(9, 13);
        tree.addChildren(10, Arrays.asList(11,12));
        tree.addChild(6, 8);
        tree.addChild(11, 14);

        Pair pair = new Pair(12, 3);
        List<Pair> pairs = Arrays.asList(pair);

        try {
            TarjanAlgorithm tarjanAlgorithm = new TarjanAlgorithm(tree);
            var result = tarjanAlgorithm.getAncestorFor(pairs);
            Assertions.assertEquals(0, result.get(pair).getId());
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail(e.getMessage());
        }
    }
}
