package pl.kaliszewski.algorithms.UnionFind;

import Graph.UnionFindPathCompression;
import javafx.application.Application;
import javafx.stage.Stage;
import pl.kaliszewski.GraphVisualization;

import java.util.Arrays;
import java.util.List;

public class UnionFind extends Application {

    private GraphVisualization visualization;

    @Override
    public void start(Stage stage) throws Exception {
        visualization = new GraphVisualization();
        List<Integer> set = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        UnionFindPathCompression unionFindPathCompression = new UnionFindPathCompression(set);
        //1 zbiór
        unionFindPathCompression.union(5, 9);
        unionFindPathCompression.union(5, 1);

        //2 zbiór
        unionFindPathCompression.union(0, 4);
        unionFindPathCompression.union(4, 3);
        unionFindPathCompression.union(3, 2);
        unionFindPathCompression.union(3, 6);

        //3 zbiór
        unionFindPathCompression.union(7, 8);

        visualization.createVisualization(unionFindPathCompression.convertToGraph(), "3 zbiory osobno");

        unionFindPathCompression.union(1, 0);
        unionFindPathCompression.union(7, 0);
        GraphVisualization secoGraphVisualization = new GraphVisualization();
        secoGraphVisualization.createVisualization(unionFindPathCompression.convertToGraph(), "Połączone");

        System.out.println(unionFindPathCompression.find(1));

       GraphVisualization thirdGraphVisualization = new GraphVisualization();
       thirdGraphVisualization.createVisualization(unionFindPathCompression.convertToGraph(), "Find 1");
    }
}
