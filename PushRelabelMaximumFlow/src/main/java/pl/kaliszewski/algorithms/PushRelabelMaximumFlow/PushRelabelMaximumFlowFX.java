package pl.kaliszewski.algorithms.PushRelabelMaximumFlow;

import Graph.MaximumFlow.PushRebelMaximumFlowGraph;
import javafx.application.Application;
import javafx.stage.Stage;
import pl.kaliszewski.GraphVisualization;

public class PushRelabelMaximumFlowFX extends Application {

    private GraphVisualization visualization;

    @Override
    public void start(Stage stage) throws Exception {
        visualization = new GraphVisualization();
        PushRebelMaximumFlowGraph pushRebelMaximumFlowGraph = new PushRebelMaximumFlowGraph(7);
        for (int i = 0; i < 7; i++) {
            pushRebelMaximumFlowGraph.addNode();
        }

        pushRebelMaximumFlowGraph.addNeighborForNode(0, 1, 90, false);
        pushRebelMaximumFlowGraph.addNeighborForNode(0, 2, 9, false);

        pushRebelMaximumFlowGraph.addNeighborForNode(1, 3, 3, false);
        pushRebelMaximumFlowGraph.addNeighborForNode(1, 4, 7, false);

        pushRebelMaximumFlowGraph.addNeighborForNode(2, 3, 3, false);
        pushRebelMaximumFlowGraph.addNeighborForNode(2, 5, 6, false);

        pushRebelMaximumFlowGraph.addNeighborForNode(3, 4, 4, false);
        pushRebelMaximumFlowGraph.addNeighborForNode(3, 6, 9, false);
        pushRebelMaximumFlowGraph.addNeighborForNode(3, 5, 2, false);

        pushRebelMaximumFlowGraph.addNeighborForNode(4, 6, 6, false);

        pushRebelMaximumFlowGraph.addNeighborForNode(5, 6, 8, false);

        System.out.println("Maksymalny przepływ: " + PushRebelMaximumFlowGraph.processPushRebelMaximumFlow(pushRebelMaximumFlowGraph,0, 6));

        visualization.createVisualization(pushRebelMaximumFlowGraph, "Maksymalny przepływ dla grafu");

    }
}
